# PARROT Local Application Product Release

The PARROT Local Application is a web application (that means that its user interface runs in your web browser) that you can install and run locally on your computer. The application provides different modules that allow you to

- run ML-models
- visualize CT images
- visualize, predict or modify segmentation information
- visualize, evaluate, and compare dose treatment plans.

You can load patient studies into the application in the form of DICOM files.Internally, the application uses an nginx server to run the web application, a Python server for the prediction of the integrated trained AI model, an Orthanc server for the storage of the study data. The installation package includes all servers. However, the Orthanc server is a manual installation. The installation instructions can be found below.

Warning: The Orthanc server will run on port 8042 of your local computer and the nginx server on port 2000.

# Installing and starting the application

- Download and unzip the installation file with 'Extract Here' or . The zip file contains a directory "parrot-app" that you should copy to your local disk, for example on your Desktop.

- To use the web application, you first have to start the included nginx server by double-clicking the file "Start_PARROT.bat" in the "parrot-app" directory.
- The process of getting the logo active for the release package:

  - Use 7zip to extract the Zip release package
  - Right-click on the 'parrot_app' application folder to open the 'Properties' window.
  - Select the 'Customise' tab. Simply click the 'OK' button to update the logo icon

- Now you can open the application's user interface in the Firefox web browser by entering the URL "localhost:2000". Note: At the moment, only Firefox is supported.
- The last step is to install the Orthanc server version 1.8.2. Go to the "Home" view in the application (in Firefox), click on "Install Orthanc", and follow the instructions. If you have already installed Orthanc on your computer you can check its status at [http://localhost:8042/system](http://localhost:8042/system). Please note that only version 1.8.2 is supported.

# Stopping the application server

- The nginx server will continue running, even when you close the web browser window. To completely stop the server, double click the "Stop_EHCARP.bat" file in the "parrot-app" directory (or stop the nginx processes in the Task Manager).
- The Orthanc server runs as a Windows service and has to be stopped in the Task Manager (in the tab "Services"). Don't forget to start the service again when you want to use the PARROT application.

# Directory structure

The "parrot-app" directory contains the following files:

- "aiIntegration" contains the integrated trained AI models, the Python server environment and the API server directories
- "dist" contains the PARROT web application
- "logs" contains the log files of the nginx server
- "temp" contains the temporary files of nginx
- "tempai" contains the results of running the Python code. The user can configure the path for AI integration.
- "nginx.exe" starts the nginx server (see the instructions above)
- "Start_PARROT.bat" start the nginx server (see the instructions above)
- "Stop_PARROT.bat" stop the ngnix server (see the instructions above)
