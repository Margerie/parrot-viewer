const fse = require('fs-extra');
const fs = require('fs');
var execSync = require('child_process').execSync;

const webConfigPath = './dist/parrot-app';

// if (fs.existsSync(webConfigPath)){
//     fs.unlinkSync(webConfigPath);
// }

fse.copySync('appconfig/nginx', webConfigPath);
fse.copySync('platform/aiIntegration', webConfigPath + '/aiIntegration');
fs.mkdirSync(webConfigPath + '/temp', { recursive: true });
fs.mkdirSync(webConfigPath + '/tempai', { recursive: true });
fs.mkdirSync(webConfigPath + '/logs', { recursive: true });

// needs to exe on cmd (folder Parrot-app): attrib +h +s parrot-app\desktop.ini
execSync('attrib +h +s .\\dist\\parrot-app\\desktop.ini');
execSync('attrib +r .\\dist\\parrot-app');
