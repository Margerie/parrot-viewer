It is necessary to change the lib 'shortId' used for the chonky library in order to create the release package:

1. Go to 'node-module' => 'shortid' module => lib => generate.js
2. Change the line: var format to 'var format = require('./format')'
3. Copy the file 'format.js' in this folder to the folder 'shortid/lib'.

Remark: We have to use the modified format.js file
