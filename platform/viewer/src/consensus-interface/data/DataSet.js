import uniqBy from 'lodash/uniqBy';

import ImageDataSet from './ImageDataSet.js';
import SegDataSet from './SegDataSet.js';
import DoseDataSet from './DoseDataSet.js';
import ContourModificationHistoryDataSet from './ContourModificationHistoryDataSet';
import DicomDataLoader from './DicomDataLoader.js';

export default class DataSet {
  static instance = null;
  imageDataSet = null;
  segDataSet = null;
  doseDataSet = null;
  contourModificationHistoryDataSet = null;
  dicomDataLoader = DicomDataLoader.getInstance(); //Default
  progressArray = [];
  listeners = [];
  study = null;

  static getInstance() {
    if (!DataSet.instance) DataSet.instance = new DataSet();

    return this.instance;
  }

  setDataSet(dataSet) {
    this.imageDataSet = dataSet.getImageDataSet();
    this.segDataSet = dataSet.getSegDataSet();
    this.doseDataSet = dataSet.getDoseDataSet();
    this.contourModificationHistoryDataSet = dataSet.getContourModificationHistoryDataSet();
    this.study = dataSet.getStudy();
    this.dicomDataLoader = dataSet.getDataLoader();

    const forwardListener = (event) => {
      this.modified(event);
    };
    dataSet.addListener(forwardListener);
  }

  getDataLoader() {
    return this.dicomDataLoader;
  }

  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;

    if (this.imageDataSet) this.imageDataSet.setDataLoader(dataLoader);

    if (this.segDataSet) this.segDataSet.setDataLoader(dataLoader);

    if (this.doseDataSet) this.doseDataSet.setDataLoader(dataLoader);

    if (this.contourModificationHistoryDataSet) {
      this.contourModificationHistoryDataSet.setDataLoader(dataLoader);
    }
  }

  //StudyInstanceUID is mandatory. If mixed StudyInstanceUID are allowed. Put whatever you want
  async setData(series, segs, StudyInstanceUID) {
    await this.delete();

    this.imageDataSet = new ImageDataSet();
    this.segDataSet = new SegDataSet();
    this.doseDataSet = new DoseDataSet();
    this.contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();

    this.imageDataSet.setDataLoader(this.dicomDataLoader);
    this.segDataSet.setDataLoader(this.dicomDataLoader);
    this.doseDataSet.setDataLoader(this.dicomDataLoader);
    this.contourModificationHistoryDataSet.setDataLoader(this.dicomDataLoader);

    this.study = [];
    series.forEach((seriesEl) => seriesEl.metaData.forEach((instance) => this.study.push(instance)));

    event = {
      target: StudyInstanceUID,
      type: 'created',
      value: null,
    };
    this.modified(event);

    const imageListener = (event) => {
      this.modified(event);
    };
    this.imageDataSet.addListener(imageListener);

    series.forEach((seriesEl) => this.imageDataSet.setData(seriesEl.metaData, seriesEl.data));

    //TODO: segs
    //this.study.append(segs.map(seriesEl => seriesEl.metaData));
  }

  async loadStudyAsync(StudyInstanceUID) {
    await this.delete();

    this.imageDataSet = new ImageDataSet();
    this.segDataSet = new SegDataSet();
    this.doseDataSet = new DoseDataSet();
    this.contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();

    this.imageDataSet.setDataLoader(this.dicomDataLoader);
    this.segDataSet.setDataLoader(this.dicomDataLoader);
    this.doseDataSet.setDataLoader(this.dicomDataLoader);
    this.contourModificationHistoryDataSet.setDataLoader(this.dicomDataLoader);

    // Images
    this.dicomDataLoader.enable();

    this.study = await this.dicomDataLoader.getStudyMetaDataFast(StudyInstanceUID);

    event = {
      target: StudyInstanceUID,
      type: 'created',
      value: null,
    };
    this.modified(event);

    const SeriesInstanceUIDs = uniqBy(this.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'CT' || study.modalities == 'MR' || study.modalities == 'PT')
      .map((study) => study.SeriesInstanceUID);

    const progressFunc = (SeriesInstanceUID, progess) => {
      const index = this.progressArray.map((elem) => elem.SeriesInstanceUID).indexOf(SeriesInstanceUID);

      if (index > -1) this.progressArray[index].progress = progess;
      else this.progressArray.push({ SeriesInstanceUID, progess });

      event = {
        target: SeriesInstanceUID,
        type: 'progress',
        value: progess,
      };
      this.modified(event);
    };

    const imageListener = (event) => {
      this.modified(event);
    };
    this.imageDataSet.addListener(imageListener);

    let loadingFuncs = SeriesInstanceUIDs.map((SeriesInstanceUID) =>
      this.imageDataSet.loadDataAsync(StudyInstanceUID, SeriesInstanceUID, (progress) => {
        progressFunc(SeriesInstanceUID, progress);
      })
    );

    // Segs
    const segListener = (event) => {
      this.modified(event);
    };
    this.segDataSet.addListener(segListener);

    let loadingFuncs2 = [];

    const SEGSeriesUIDs = uniqBy(this.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'SEG' || study.modalities == 'RTSTRUCT')
      .map((study) => study.SeriesInstanceUID);
    const SEGSOPUIDs = uniqBy(this.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'SEG' || study.modalities == 'RTSTRUCT')
      .map((study) => study.SOPInstanceUID);

    if (SEGSeriesUIDs) {
      loadingFuncs2 = SEGSeriesUIDs.map((SegSeriesUID, index) => this.segDataSet.loadDataAsync(StudyInstanceUID, SegSeriesUID, SEGSOPUIDs[index]));
    }

    /// Dose
    const doseListener = (event) => {
      this.modified(event);
    };
    this.doseDataSet.addListener(doseListener);

    let loadingFuncs3 = [];

    const RTDOSESeriesUIDs = uniqBy(this.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'RTDOSE')
      .map((study) => study.SeriesInstanceUID);
    const RTDOSESOPUIDs = uniqBy(this.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'RTDOSE')
      .map((study) => study.SOPInstanceUID);

    if (RTDOSESeriesUIDs) {
      loadingFuncs3.push(RTDOSESeriesUIDs.map((DoseSeriesUID, index) => this.doseDataSet.loadDataAsync(StudyInstanceUID, DoseSeriesUID, RTDOSESOPUIDs[index])));
    }

    /// Contour modification history
    const contourModificationHistoryListener = (event) => {
      this.modified(event);
    };
    this.contourModificationHistoryDataSet.addListener(contourModificationHistoryListener);

    let loadingFuncs4 = [];
    loadingFuncs4.push(this.contourModificationHistoryDataSet.loadDataAsync(StudyInstanceUID));

    // wait that all loading functions finish
    await Promise.all(loadingFuncs.concat(loadingFuncs2).concat(loadingFuncs3).concat(loadingFuncs4));

    // Calculate the data for the segments based on last modification
    const allSegments = this.segDataSet.getAllSegs().flatMap((segs) => segs.getSegs());
    await Promise.all(
      allSegments.map(async (seg) => {
        const { data, attachmentID } = await this.contourModificationHistoryDataSet.getSegmentData(seg, -1, StudyInstanceUID);
        seg.prepareVTKData(data, attachmentID);
      }, this)
    );
  }

  getStudy() {
    return this.study;
  }

  getImageDataSet() {
    return this.imageDataSet;
  }

  getSegDataSet() {
    return this.segDataSet;
  }

  getDoseDataSet() {
    return this.doseDataSet;
  }

  getContourModificationHistoryDataSet() {
    return this.contourModificationHistoryDataSet;
  }

  modified(event) {
    this.listeners.forEach((listener) => listener(event));
  }

  addListener(listener) {
    this.listeners.push(listener);
  }

  removeListener(listener) {
    const index = this.listeners.indexOf(listener);

    if (index > -1) this.listeners.splice(index, 1);
  }

  prioritizeSeriesUID(seriesUID) {
    //TODO
  }

  async delete() {
    if (this.imageDataSet) await this.imageDataSet.delete(); // Will also remove listeners
    if (this.segDataSet) await this.segDataSet.delete(); // Will also remove listeners
    if (this.doseDataSet) await this.doseDataSet.delete(); // Will also remove listeners
    if (this.contourModificationHistoryDataSet) await this.contourModificationHistoryDataSet.delete();

    this.study = null;
    this.imageDataSet = null;
    this.segDataSet = null;
    this.doseDataSet = null;
    this.contourModificationHistoryDataSet = null;
    this.progressArray = [];
    this.listeners = [];
  }
}
