import React from 'react';
import { Modal } from 'antd';
import { LinearProgress } from '@mui/material';
import DataSet from './../../data/DataSet.js';
import ContourModificationHistoryDataSet from '../../data/ContourModificationHistoryDataSet';
import { FiDelete } from 'react-icons/fi';
import { BiBookContent } from 'react-icons/bi';
import { SETTING_NAMES, MainSettings } from '../../utils/MainSettings.js';
import UploadDicomFileDialog from './UploadDicomFileDialog.js';
import StudyContentDialog from './StudyContentDialog.js';
import { confirmAlert } from 'react-confirm-alert';
// import 'react-confirm-alert/src/react-confirm-alert.css';
import './styles.css';

var jsZip = require('jszip');

class StudyManagementTable extends React.Component {
  constructor(props) {
    super(props);
  }

  renderTableHeader(headerName) {
    return (
      <th
        className={'studyManagement' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
        }}
      >
        {headerName}
      </th>
    );
  }

  openStudy(rowItem) {
    this.props.onClickOpenStudy(rowItem.StudyInstanceUID);
  }

  deleteStudy(rowItem, event) {
    this.props.onClickDeleteStudy(rowItem.StudyInstanceUID, event);
  }

  closeStudyContentDialog() {
    this.props.closeStudyContentDialog();
  }

  callOpenSelectedStudyContentDialog(item) {
    this.props.openStudyContentDialog(item.StudyInstanceUID);
  }

  handlerLoadButtonText(item) {
    if (this.props.activeStudy == item.StudyInstanceUID && !this.props.isLoading) {
      return 'Loaded';
    } else {
      return 'Load';
    }
  }

  renderStudyContentIconTD(item, style) {
    return (
      <td className="actionShowStudyContentTD" style={style}>
        <BiBookContent
          className={'showStudyContentRowIcon' + item.PatientName}
          value={item.PatientName}
          title="Show the study content"
          onClick={() => this.callOpenSelectedStudyContentDialog(item)}
          style={{
            fontSize: '25px',
            cursor: 'pointer',
          }}
        />
      </td>
    );
  }

  renderStudyManagementTBody() {
    const style = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'center',
    };
    return (
      <tbody className="studyManagementTableTBody">
        {this.props.studies.map((item) => (
          <tr className={this.props.activeStudy === item.StudyInstanceUID ? 'tableRowSelected' : 'noSelectedRow'}>
            {this.renderStudyContentIconTD(item, style)}
            <td
              className={'actionUploadingStudyTD' + item.PatientName}
              style={{
                padding: '1px',
                border: 'solid 1px grey',
                fontSize: '14px',
                textAlign: 'center',
                cursor: 'pointer',
              }}
              title="Loading study from the backend orthanc server. A study can be visualized and manipulated in the other two modules"
              onClick={() => this.openStudy(item)}
            >
              {this.handlerLoadButtonText(item)}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.PatientName}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.PatientID}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.PatientBirthdate}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.patientSex}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.StudyDate}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.StudyID}
            </td>
            <td style={style} value={item.StudyInstanceUID}>
              {item.ReferringPhysicianName}
            </td>
            <td className="actionDeleteStudyTD">
              <FiDelete
                className={'deleteStudyRowIcon' + item.PatientName}
                value={item.PatientName}
                title="Delete the selected study from the application"
                style={{
                  fontSize: '25px',
                  cursor: 'pointer',
                }}
                onClick={(event) => {
                  this.deleteStudy(item, event);
                }}
              />
            </td>
          </tr>
        ))}
      </tbody>
    );
  }

  render() {
    return (
      <div>
        <table
          className="studyManagmentTable"
          style={{
            border: 'sold 1px grey',
            fontSize: '14px',
            padding: '2px',
            textAlign: 'center',
            borderCollapse: 'inherit',
            width: '100%',
          }}
        >
          <thead>
            <tr className="studyManagementTableHeaderTR">
              {this.renderTableHeader('')}
              {this.renderTableHeader('')}
              {this.renderTableHeader('Patient')}
              {this.renderTableHeader('Patient ID')}
              {this.renderTableHeader('Birthday')}
              {this.renderTableHeader('Sex')}
              {this.renderTableHeader('Study Date')}
              {this.renderTableHeader('Study ID')}
              {this.renderTableHeader('Physician Name')}
              {this.renderTableHeader('')}
            </tr>
          </thead>
          {this.renderStudyManagementTBody()}
        </table>
      </div>
    );
  }
}

export default class StudyDataManagement extends React.Component {
  constructor(props) {
    super(props);

    const settings = new MainSettings();
    const pacsURL = settings.get(SETTING_NAMES.PACS_URL);
    this.state = {
      activeStudyInstanceUID: this.props.activeStudyInstanceUID,
      studies: null,
      StudyInstanceUID: null,
      isLoading: false,
      isConfirmingDelete: false,
      isDeleting: false,
      uploadedList: [],
      studyMetaData: null,
      openedStudyContentUID: null,
    };

    this.dicomDataLoader = DataSet.getInstance().getDataLoader();
    this.dicomDataLoader.setPACSURL(pacsURL);
    this.dicomDataLoader.enable();
    this.loadStudyList();

    this.selectStudyFunctions = {};
    this.uploadStudyFunctions = {};
    this.uploadDicomFileFunctions = {};
    this.studyContentDialogFunction = {};
  }

  loadStudyList() {
    this.dicomDataLoader
      .getStudies()
      .then((studies) => {
        if (studies) {
          this.setState({
            studies: studies.slice(),
          });
        } else {
          this.setState({
            studies: null,
          });
        }
      })
      .catch((error) => {
        return confirmAlert({
          title: 'Get studies',
          message:
            'Unable to read the list of studies. Have you installed the Orthanc server? You can install the server by following the link on the home page',
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
              onClick: () => {
                this.studyContentDialogFunction.setVisible(false);
              },
            },
          ],
        });
      });
  }

  handleClickOpenStudy(StudyInstanceUID) {
    this.setState({
      StudyInstanceUID: StudyInstanceUID,
      isLoading: true,
    });
    DataSet.getInstance()
      .loadStudyAsync(StudyInstanceUID)
      .finally(() => {
        this.setState({
          isLoading: false,
        });
      });
    this.props.setStudy(StudyInstanceUID);
    this.setState({
      activeStudyInstanceUID: StudyInstanceUID,
    });
  }

  async deleteStudy(siuid) {
    const listOfStudyIDs = await (await fetch('/getstudy', { method: 'GET' })).json();
    for (var i = 0; i < listOfStudyIDs.length; i++) {
      const studyInfo = await (await fetch('/getstudy/' + listOfStudyIDs[i], { method: 'GET' })).json();
      if (studyInfo.MainDicomTags.StudyInstanceUID == siuid) {
        await fetch('/deletestudy/' + listOfStudyIDs[i], { method: 'DELETE' });
        break;
      }
    }
  }

  handleClickDeleteStudy(siuid) {
    this.setState({
      isConfirmingDelete: true,
      studyIUIDDelete: siuid,
    });
  }

  handleConfirmedDeleteStudy() {
    const siuid = this.state.studyIUIDDelete;
    this.setState({
      isDeleting: true,
    });
    // unload the study
    if (siuid == this.state.activeStudyInstanceUID) {
      this.setState({
        StudyInstanceUID: null,
        activeStudyInstanceUID: null,
      });
      DataSet.getInstance()
        .delete()
        .then(() => {
          this.dicomDataLoader.enable();
        });
    }
    // delete in server
    this.deleteStudy(siuid).finally(() => {
      this.setState({
        isDeleting: false,
      });
      this.loadStudyList();
    });
  }

  fileUploadSuccessfull(filename) {
    const newList = this.state.uploadedList.slice();
    newList.push({
      fileName: filename,
      status: true,
    });
    this.setState({
      uploadedList: newList,
    });
    this.checkUploadFinished(newList.length);
  }

  fileUploadFailed(filename) {
    const newList = this.state.uploadedList.slice();
    newList.push({
      fileName: filename,
      status: false,
    });
    this.setState({
      uploadedList: newList,
    });
    this.checkUploadFinished(newList.length);
  }

  checkUploadFinished(numberOfUploadedFiles) {
    if (numberOfUploadedFiles == this.totalNumberOfFilesToUpload) {
      // all files uploaded.
      // we process the modification history file now
      if (this.uploadedHistoryData) {
        const contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();
        contourModificationHistoryDataSet
          .getOrthancInstanceID(this.uploadedHistoryData.StudyInstanceUID)
          .then((iid) => {
            // study exists?
            if (iid == null) {
              throw Error('Study does not exist');
            } else {
              // load history from uploaded data
              return contourModificationHistoryDataSet.loadDataAsync(this.uploadedHistoryData.StudyInstanceUID).then(() => {
                return contourModificationHistoryDataSet.setContourModifications(this.uploadedHistoryData.StudyInstanceUID, this.uploadedHistoryData.history);
              });
            }
          })
          .catch((error) => {
            this.setState({ uploadedHistoryCreationFailed: true });
          })
          .finally(() => {
            contourModificationHistoryDataSet.delete();
            // we don't need the data anymore
            this.uploadedHistoryData = null;
          });
      }
    }
  }

  uploadSingleFileToServer(filename, data) {
    console.log('Uploading ' + filename);

    if (filename.toUpperCase().endsWith('JSON')) {
      // file is probably a modification history file.
      data
        .text()
        .then((text) => {
          const historyData = JSON.parse(text);
          if (!historyData.StudyInstanceUID || !historyData.history) {
            throw Error('Invalid history file');
          } else if (historyData.StudyInstanceUID == this.state.activeStudyInstanceUID) {
            this.setState({ isUploadingHistoryOfActiveStudy: true });
            throw Error('Uploaded history belongs to active study');
          } else {
            const contourModificationHistoryDataSet = new ContourModificationHistoryDataSet();
            contourModificationHistoryDataSet
              .loadDataAsync(historyData.StudyInstanceUID)
              .then(() => {
                if (contourModificationHistoryDataSet.getHistoryInfoData().length > 0) {
                  this.setState({ studyAlreadyHasHistory: true });
                  // the throw is not catched. why?
                  // throw Error("Study already has history")
                  this.fileUploadFailed(filename);
                } else {
                  this.uploadedHistoryData = historyData;
                  this.fileUploadSuccessfull(filename);
                }
              })
              .catch((error) => {
                throw error;
              })
              .finally(() => {
                contourModificationHistoryDataSet.delete();
              });
          }
        })
        .catch((error) => {
          this.fileUploadFailed(filename);
        });
    } else {
      // file is probably a dicom file.
      // we upload it to the orthanc server using the POST /uploaddicom function
      const formData = new FormData();
      formData.append('files[]', data);
      const requestOptions = {
        method: 'POST',
        headers: {},
        body: formData,
      };
      fetch('/uploaddicom', requestOptions)
        .then((response) => {
          if (response.ok) {
            this.fileUploadSuccessfull(filename);
          } else {
            throw Error('upload failed');
          }
        })
        .catch((error) => {
          this.fileUploadFailed(filename);
        });
    }
  }

  uploadZipFileToServer(zipfile) {
    const self = this;
    jsZip.loadAsync(zipfile).then(function (zip) {
      zip.forEach(function (filepath) {
        // upload the file
        const f = zip.file(filepath);
        // f is null if filepath is a directory
        if (f) {
          // increase the number of files to upload by 1
          self.totalNumberOfFilesToUpload++;
          self.setState({ numFilesToUpload: self.totalNumberOfFilesToUpload });
          // upload the file content to the server
          f.async('blob').then(function (blob) {
            self.uploadSingleFileToServer(filepath, blob);
          });
        }
      });
      self.totalNumberOfFilesToUpload--; // don't count the zip file
      self.setState({ numFilesToUpload: self.totalNumberOfFilesToUpload });
    });
  }

  onStudyUploadHandler(event) {
    const files = event.target.files;
    if (files && files.length) {
      this.setState({
        uploadedList: [],
        numFilesToUpload: files.length,
      });
      this.uploadDicomFileFunctions.setVisible(true);

      // number of files selected by the user to upload.
      // when we find zip files and upload their content, the total
      // number of files to upload will increase
      this.totalNumberOfFilesToUpload = files.length;

      // when we find a json file with history data we put it here:
      this.uploadedHistoryData = null;

      for (var i = 0; i < files.length; i++) {
        const filename = files[i].name.trim();
        if (filename.toUpperCase().endsWith('ZIP')) {
          this.uploadZipFileToServer(files[i]);
        } else {
          this.uploadSingleFileToServer(filename, files[i]);
        }
      }
    }
  }

  catchLoadStudyMetaDataError(error, errorType) {
    return confirmAlert({
      title: 'Show the study metadata and downloand features',
      message: 'Loading study content failed: ' + errorType,
      closeOnEscape: false,
      closeOnClickOutside: false,
      buttons: [
        {
          label: 'OK',
          onClick: () => {
            this.studyContentDialogFunction.setVisible(false);
          },
        },
      ],
    });
  }

  async openStudyContentDialog(studyUID) {
    this.setState({
      studyMetaData: null,
      openedStudyContentUID: null,
    });
    this.studyContentDialogFunction.setVisible(true);

    const meta = await this.dicomDataLoader.getStudyMetaData(studyUID).catch((error) => {
      this.catchLoadStudyMetaDataError(error, 'Meta data');
    });
    let downloadInfoData = [];
    const listOfStudyIDs = await (await fetch('/getstudy', { method: 'GET' })).json().catch((error) => {
      this.catchLoadStudyMetaDataError(error, 'Get Instance IDs');
    });

    for (let t = 0; t < listOfStudyIDs.length; t++) {
      const studyInfo = await (await fetch('/getstudy/' + listOfStudyIDs[t], { method: 'GET' })).json().catch((error) => {
        this.catchLoadStudyMetaDataError(error, 'Get Instance IDs');
      });

      if (studyInfo.MainDicomTags.StudyInstanceUID == studyUID) {
        const series = studyInfo.Series;
        for (let s = 0; s < series.length; s++) {
          const instances = await (await fetch('/getInstances/' + series[s], { method: 'GET' })).json().catch((error) => {
            this.catchLoadStudyMetaDataError(error, 'Get Instance IDs');
          });

          const dicomTages = instances.MainDicomTags;
          if (dicomTages.Modality == 'RTDOSE' || dicomTages.Modality == 'RTPLAN' || dicomTages.Modality == 'RTSTRUCT' || dicomTages.Modality == 'REG') {
            const data = { seriesIUID: dicomTages.SeriesInstanceUID, modality: dicomTages.Modality, instanceID: instances.Instances[0] };
            downloadInfoData.push(data);
          }
        }
      }
    }

    this.setState({
      studyMetaData: { metaData: meta, downloadInfoData: downloadInfoData },
      openedStudyContentUID: studyUID,
    });
  }

  renderStudyManagementTable(studies) {
    if (studies) {
      return (
        <StudyManagementTable
          className="studyManagementTable"
          studies={studies}
          onClickOpenStudy={(StudyInstanceUID) => this.handleClickOpenStudy(StudyInstanceUID)}
          onClickDeleteStudy={(StudyInstanceUID, event) => this.handleClickDeleteStudy(StudyInstanceUID, event)}
          selectStudyFunctions={this.selectStudyFunctions}
          activeStudy={this.state.activeStudyInstanceUID}
          openStudyContentDialog={(id) => this.openStudyContentDialog(id)}
          isLoading={this.state.isLoading}
        ></StudyManagementTable>
      );
    }
  }

  render() {
    const patientName = this.state.studies?.find((study) => study.StudyInstanceUID == this.state.StudyInstanceUID)?.PatientName;
    return (
      <div>
        <div className="studyManagementButtonsDiv">
          <input
            id="uploadStudyInputId"
            type="file"
            ref={this.uploadFilesDirPath}
            name="uploadedFiles"
            multiple
            hidden
            onChange={(event) => this.onStudyUploadHandler(event)}
          />
          <label htmlFor="uploadStudyInputId" className="studyManagementButtonUpload">
            Upload Study
          </label>
        </div>
        {this.renderStudyManagementTable(this.state.studies)}
        <Modal className="loadingPreviewDialog" closable={false} visible={this.state.isLoading} footer={null}>
          <h3 className="loadingStudyPatientlabel">Loading study of patient {patientName} ... </h3>
          <LinearProgress className="linearProgress"></LinearProgress>
        </Modal>
        <Modal
          className="confirmDeleteStudyDialog"
          closable={false}
          open={this.state.isConfirmingDelete}
          onCancel={() =>
            this.setState({
              isConfirmingDelete: false,
            })
          }
          onOk={() => {
            this.setState({ isConfirmingDelete: false });
            this.handleConfirmedDeleteStudy();
          }}
          style={{ width: 'fit-content' }}
        >
          <h3 className="confirmDeleteStudylabel" style={{ color: 'yellow', textAlign: 'center', fontSize: '25px' }}>
            Do you really want to delete the study?
          </h3>
        </Modal>
        <Modal className="deletingDialog" closable={false} open={this.state.isDeleting} footer={null} style={{ width: 'fit-content' }}>
          <h3 className="deletingStudyPatientlabel" style={{ textAlign: 'center', color: 'red', fontSize: '25px' }}>
            Deleting Study...
          </h3>
        </Modal>
        <Modal
          className="confirmUploadHistoryFileActiveStudyDialog"
          closable={false}
          open={this.state.isUploadingHistoryOfActiveStudy}
          zIndex={1000}
          cancelButtonProps={{ style: { display: 'none' } }}
          onOk={() => {
            this.setState({ isUploadingHistoryOfActiveStudy: false });
          }}
        >
          <h3 className="confirmUploadHistoryActiveStudylabel">
            The modification history that you are trying to upload belongs to the currently active study and has therefore been ignored. Uploading it would
            result in inconsistent modifications. Please reload the webpage to unload the currently active study and try again.
          </h3>
        </Modal>
        <Modal
          className="confirmUploadHistoryAlreadyModifiedDialog"
          closable={false}
          open={this.state.studyAlreadyHasHistory}
          zIndex={1000}
          cancelButtonProps={{ style: { display: 'none' } }}
          onOk={() => {
            this.setState({ studyAlreadyHasHistory: false });
          }}
        >
          <h3 className="confirmUploadHistoryAlreadyModifiedLabel">
            The modification history that you are trying to upload belongs to an existing study that already has modifications. To replace the modification
            history, either delete the existing modifications or delete and reupload the entire study.
          </h3>
        </Modal>
        <Modal
          className="confirmUploadHistoryNoProcessedDialog"
          closable={false}
          open={this.state.uploadedHistoryCreationFailed}
          zIndex={1000}
          cancelButtonProps={{ style: { display: 'none' } }}
          onOk={() => {
            this.setState({ uploadedHistoryCreationFailed: false });
          }}
        >
          <h3 className="confirmUploadHistoryNoProcessedLabel">
            The modification history that you have uploaded could not be processed. Please verify that it belongs to one of the existing studies.
          </h3>
        </Modal>
        <UploadDicomFileDialog
          className="uploadDicomFileDialog"
          uploadDicomFileFunctions={this.uploadDicomFileFunctions}
          uploadedList={this.state.uploadedList}
          numFilesToUpload={this.state.numFilesToUpload}
          onOk={() => this.loadStudyList()}
        ></UploadDicomFileDialog>
        <StudyContentDialog
          className="studyContentDialog"
          studyContentDialogFunction={this.studyContentDialogFunction}
          studyMetaData={this.state.studyMetaData}
          studies={this.state.studies}
          openedStudyContentUID={this.state.openedStudyContentUID}
          activeStudy={this.state.activeStudyInstanceUID}
          onReloadActiveStudy={() => this.handleClickOpenStudy(this.state.activeStudyInstanceUID)}
        ></StudyContentDialog>
      </div>
    );
  }
}
