import { Button, Modal } from 'antd';
import React from 'react';
import 'react-confirm-alert/src/react-confirm-alert.css';
import { BsCheckSquare, BsXSquare } from 'react-icons/bs';
import './styles.css';

class UplodadFileTable extends React.Component {
  constructor(props) {
    super(props);
  }

  renderTableHeader(headerName) {
    return (
      <th
        className={'uploadFileTable' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px balck',
          textAlign: 'center',
          fontSize: '12px',
          padding: '2px',
          background: 'grey',
          color: 'white',
        }}
      >
        {headerName}
      </th>
    );
  }

  renderUploadFileTBody() {
    const style = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'center',
    };
    return (
      <tbody className="uploadFileTableTBody">
        {this.props.uploadedList.map((item) => (
          <tr className="uploadFileTbodyTR">
            <td className="uploadFileTableFileNameTD" style={style}>
              {item.fileName}
            </td>
            <td className="uploadFileTableStatusTD" style={style}>
              {item.status ? (
                <BsCheckSquare className={'sendingCheckIcon'} style={{ fontSize: '20px' }}></BsCheckSquare>
              ) : (
                <BsXSquare className={'sendingXIcon'} style={{ fontSize: '20px' }}></BsXSquare>
              )}
            </td>
          </tr>
        ))}
      </tbody>
    );
  }

  render() {
    return (
      <table
        className="uploadFileTable"
        style={{
          border: 'sold 1px grey',
          fontSize: '14px',
          padding: '2px',
          textAlign: 'center',
          borderCollapse: 'inherit',
          width: '100%',
        }}
      >
        <thead>
          <tr className="uploadFileTableHeaderTR">
            {this.renderTableHeader('File Name')}
            {this.renderTableHeader('Status')}
          </tr>
        </thead>
        {this.renderUploadFileTBody()}
      </table>
    );
  }
}

export default class UploadDicomFileDialog extends React.Component {
  constructor(props) {
    super(props);

    props.uploadDicomFileFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
    };
  }

  componentDidUpdate(prevProps) {
    this.props.uploadDicomFileFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  handleOK() {
    this.setVisible(false);
    this.props.onOk();
  }

  render() {
    return (
      <Modal
        className="uploadDicomFilesDialog"
        visible={this.state.visible}
        closable={false}
        destroyOnClose={true}
        onOk={() => this.handleOK()}
        footer={[
          <Button key="ok" type="primary" onClick={() => this.handleOK()} disabled={this.props.uploadedList.length < this.props.numFilesToUpload}>
            {this.props.uploadedList.length < this.props.numFilesToUpload ? 'Uploading' : 'OK'}
          </Button>,
        ]}
      >
        <div>
          <h2 className="selectDicomFileTitle">Uploading DICOM Files</h2>
          <h3 className="uploadingFileProcessLabel">
            Processed {this.props.uploadedList.length} of {this.props.numFilesToUpload} files
          </h3>
          <div className="uploadFileDiv">
            <UplodadFileTable className="uploadFileTable" uploadedList={this.props.uploadedList}></UplodadFileTable>
          </div>
        </div>
      </Modal>
    );
  }
}
