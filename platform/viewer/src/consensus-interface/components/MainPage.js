import React, { Component, useState } from 'react';
import Grid from '@mui/material/Grid';
import homeLogo from '../../../public/assets/homeLogo.png';
import './styles.css';

class ApplicationGeneralTable extends React.Component {
  constructor(props) {
    super(props);
  }

  renderTableHeader(headerName) {
    return (
      <th
        className={'applicationGeneralInfo' + headerName + 'TH'}
        rowSpan="1"
        style={{
          border: 'solid 1px black',
          textAlign: 'center',
          fontSize: '16px',
          padding: '2px',
          background: 'grey',
          color: 'white',
        }}
      >
        {headerName}
      </th>
    );
  }

  renderTableTBody() {
    const style = {
      padding: '1px',
      border: 'solid 1px grey',
      fontSize: '14px',
      textAlign: 'center',
      width: '100px',
    };

    const tdStyle = {
      padding: '8px',
      border: '1px solid grey',
      fontSize: '16px',
      textAlign: 'center',
      color: 'white',
    };
    return (
      <tbody className="applicationGeneralTableTBody">
        <tr>
          <td className="clinialGoalTemplateTD" style={tdStyle}>
            PARROT requires the clinical goals to follow the provided template
          </td>
          <td className="downloadClinicalGoalTemplateTD" style={tdStyle}>
            <a href="/template/Clinical_Goal_Template.xlsx" download style={{ color: 'white' }}>
              Clinical goals template
            </a>
          </td>
        </tr>
        <tr>
          <td className="contourNameStandardTD" style={tdStyle}>
            PARROT NTCP plugin requires the name of some organs to follow the template
          </td>
          <td className="downloadContourNameStandardTD" style={tdStyle}>
            <a href="/template/ROI_Name_Standard.xlsx" target="_self" style={{ color: 'white' }} download>
              Contour Name standard
            </a>
          </td>
        </tr>
        <tr>
          <td className="orthancInformationTD" style={tdStyle}>
            PARROT requires the use of Orthanc version 1.12.0
            <br />
            <a href="http://localhost:8042/system" target="_self" style={{ color: '#F28227' }}>
              Check the Orthanc version: http://localhost:8042/system
            </a>
          </td>
          <td className="downloadOrthancTD" style={tdStyle}>
            <a href="/software/OrthancInstaller-Win64-latest.exe" target="_self" style={{ color: 'white', paddingTop: '5px' }} download>
              Install Orthanc
            </a>
          </td>
        </tr>
        <tr>
          <td className="supportOpsystemTD" style={tdStyle}>
            The application is currently running on the OS
            <br />
            <lable style={{ color: '#F28227' }}>Chrome: Ongoing</lable>
          </td>
          <td className="fireFoxeTD" style={tdStyle}>
            <div style={{ color: 'white', paddingTop: '5px' }}>Firefox</div>
          </td>
        </tr>
      </tbody>
    );
  }

  render() {
    return (
      <table
        className="applicationGeneralTable"
        style={{
          border: 'sold 1px grey',
          fontSize: '14px',
          padding: '2px',
          textAlign: 'left',
          borderCollapse: 'inherit',
          width: '100%',
          paddingBottom: '10px',
        }}
      >
        <thead>
          <tr className="applicationGeneralTableHeaderTR">
            {this.renderTableHeader('Information')}
            {this.renderTableHeader('Download')}
          </tr>
        </thead>
        {this.renderTableTBody()}
      </table>
    );
  }
}

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }

  style = {
    backgroundColor: 'white',
    color: 'black',
    border: 'solid 1px black',
    borderRadius: '5px',
    cursor: 'pointer',
    width: '270px',
    height: '48px',
  };

  render() {
    // const StartButton = withRouter(({history}) => (
    //     <button className='startAppButton'
    //         style={this.style}
    //         onClick={() => {history.push('/main')}}
    //         >Start
    //     </button>
    // ))

    const mainExplanationStyle = {
      textAlign: 'center',
      whiteSpace: 'pre-wrap',
      overflowWrap: 'break-word',
      color: 'white',
    };

    return (
      <div className="homePageDiv">
        <Grid container justifyContent="center">
          <div className="mainExplanationText" style={mainExplanationStyle}>
            <img src={homeLogo} width="350" height="180" />
          </div>
        </Grid>
        <Grid container justifyContent="center">
          <h1 className="nameAppDiv" style={{ color: '#F28227' }}>
            PARROT Local Application
          </h1>
        </Grid>
        <Grid container justifyContent="center">
          <div className="downloadFilesDiv" style={{ paddingTop: '40px' }}>
            <ApplicationGeneralTable />
          </div>
        </Grid>
      </div>
    );
  }
}
