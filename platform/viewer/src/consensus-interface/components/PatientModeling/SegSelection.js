import React, { Component } from 'react';
import { Select } from 'antd';
import uniqBy from 'lodash/uniqBy';

// import { SETTING_NAMES, MainSettings } from '../../utils/MainSettings.js';

import './styles.css';

export default class SegSelection extends React.Component {
  constructor(props) {
    super(props);

    const study = this.props.study;
    let activeSEGSeriesUID = null;
    let activeSEGSOPUID = null;

    if (study) {
      activeSEGSeriesUID = uniqBy(study, 'SeriesInstanceUID')
        .filter((study) => study.modalities === 'SEG' || study.modalities === 'RTSTRUCT')
        .map((study) => study.SeriesInstanceUID)[0];
      activeSEGSOPUID = study.filter((study) => study.SeriesInstanceUID === activeSEGSeriesUID).map((study) => study.SOPInstanceUID)[0];
    }

    this.state = {
      activeSEGSeriesUID,
      activeSEGSOPUID,
      onSelectedSEG: this.props.onSelectedSEG,
      study,
    };

    if (study) this.props.onSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID);
  }

  getSEGUIDs() {
    if (!this.state.study) return null;

    return uniqBy(this.state.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities === 'SEG' || study.modalities === 'RTSTRUCT')
      .map((study) => study.SeriesInstanceUID);
  }

  getSEGNames() {
    if (!this.state.study) return null;

    return uniqBy(this.state.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'SEG' || study.modalities == 'RTSTRUCT')
      .map((study) => {
        return {
          SeriesDescription: study.SeriesDescription,
          SeriesInstanceUID: study.SeriesInstanceUID,
        };
      });
  }

  handleChangeActiveSEG(event) {
    const activeSEGSOPUID = event == -1 ? [-1] : this.state.study.filter((study) => study.SeriesInstanceUID === event).map((study) => study.SOPInstanceUID);
    this.state.onSelectedSEG(event, activeSEGSOPUID[0]);
    this.setState({ activeSEGSeriesUID: event, activeSEGSOPUID: activeSEGSOPUID[0] });
  }

  render() {
    const SEGNames = this.getSEGNames();
    if (!SEGNames || !SEGNames.length) return <div>No Contour Found</div>;

    const SEGNamesOptions = SEGNames.map((seg) => {
      return [
        <option key={seg.SeriesInstanceUID} value={seg.SeriesInstanceUID}>
          {seg.SeriesDescription ? seg.SeriesDescription : 'Predicted Series'}
        </option>,
      ];
    });

    SEGNamesOptions.push([
      <option key={-1} value={-1}>
        Show all
      </option>,
    ]);

    // const settings = new MainSettings();
    // const pacsURL = settings.get(SETTING_NAMES.PACS_URL);

    return (
      <div className="SegSelection">
        <Select
          className="selectSEG"
          // id="selectSEG"
          placeholder="Select SEG Series"
          style={{ color: 'white' }}
          dropdownStyle={{ backgroundColor: 'white' }}
          value={this.state.activeSEGSeriesUID}
          onChange={(event) => {
            this.handleChangeActiveSEG(event);
          }}
        >
          {SEGNamesOptions}
        </Select>
      </div>
    );
  }
}
