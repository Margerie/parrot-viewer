import React, { Component } from 'react';
import PropTypes from 'prop-types';
import vtkGenericRenderWindow from '@kitware/vtk.js/Rendering/Misc/GenericRenderWindow';
// import vtkWidgetManager from '@kitware/vtk.js/Widgets/Core/WidgetManager';
// import vtkPaintFilter from '@kitware/vtk.js/Filters/General/PaintFilter';
// import vtkPaintWidget from '@kitware/vtk.js/Widgets/Widgets3D/PaintWidget';
// import { ViewTypes } from '@kitware/vtk.js/Widgets/Core/WidgetManager/Constants';

import vtkVolume from '@kitware/vtk.js/Rendering/Core/Volume';
import vtkVolumeMapper from '@kitware/vtk.js/Rendering/Core/VolumeMapper'
import vtkColorTransferFunction from '@kitware/vtk.js/Rendering/Core/ColorTransferFunction';
import vtkPiecewiseFunction from '@kitware/vtk.js/Common/DataModel/PiecewiseFunction';

import vtkOrientationMarkerWidget from '@kitware/vtk.js/Interaction/Widgets/OrientationMarkerWidget';
// import vtkAnnotatedCubeActor from '@kitware/vtk.js/Rendering/Core/AnnotatedCubeActor';

import {applyPreset} from '../../utils/VTKUtils.js';

import vtkActor from '@kitware/vtk.js/Rendering/Core/Actor';
import vtkMapper from '@kitware/vtk.js/Rendering/Core/Mapper';
import vtkSTLReader from '@kitware/vtk.js/IO/Geometry/STLReader';
import human_standing from "../../../../public/stl/human_standing.stl";

export default class View3D extends Component {
  constructor(props) {
    super(props);

    this.activeSeriesInstanceUID = null;
    this.activeSecondarySeriesInstanceUID = null;
    
    this.imagePropertiesListener = (whatChange) => {this.onPropertyChange(whatChange)};
    
    this.state = {
      SeriesInstanceUID: this.props.SeriesInstanceUID,
      secondarySeriesInstanceUID: this.props.secondarySeriesInstanceUID,
      segDataSet: this.props.segDataSet,
    };
    
    this.listenerIds = [];
    
    this.orientationActor = null;
    this.orientationMapper = null;
    this.orientationWidget = null;
    
    this.reader = null;
    
    this.scene = {
      camera:null,
      container: React.createRef(),
      genericRenderWindow: null,
      openGLRenderWindow: null,
      renderer: null,
      renderWindow: null,
    };
  }
  
  applyPreset(preset) {
    applyPreset(this.image.actor, preset);
    
    this.scene.renderWindow.render();
  }
  
  onPropertyChange(whatChange) {
    switch  (whatChange) {
      case 'wwl':
        this.applyPreset(this.image.imageProperties.getTransferFunction());
        break;
    }
  }
  
  onSegChange(labelVolumeIndex, whatChange) {    
    switch  (whatChange) {
      case 'color': 
        this.setLabelsColor();
        break;
      case 'visible': 
        this.setLabelsColor();
        break;
      case 'active':
        if (!seg.isActive())
          break; // TODO: Deactivate
        this.setActiveLabelMap(labelVolumeIndex);
        break;
    }
  }
  
  setLabelsColor() {
    if (!this.state.segDataSet)
      return;
    
    const segs = this.state.segDataSet.getSegs();
    
    const cfun = vtkColorTransferFunction.newInstance();
    const ofun = vtkPiecewiseFunction.newInstance();
        
    ofun.addPoint(0, 0); // All 0's are fully transparent

    cfun.addRGBPoint(0, 0, 0, 0);
      
    segs.forEach((seg, index) => {
      const color = seg.getColor();
      
      if (seg.getVisible()) {
        ofun.addPoint(index+1, 1);
        cfun.addRGBPoint(index+1, color[0]/255.0, color[1]/255.0 , color[2]/255.0);
      }
      else {
        ofun.addPoint(index+1, 0);
        cfun.addRGBPoint(index+1, 0, 0, 0);
      }
    });
        
    cfun.setRange(0, segs.length);
    this.segImage.actor.getProperty().setRGBTransferFunction(0, cfun);
        
    this.segImage.actor.getProperty().setScalarOpacity(0, ofun);
    this.segImage.actor.getProperty().setInterpolationTypeToNearest();
        
    this.segImage.actor.getProperty().setScalarOpacityUnitDistance(0, 0.5);
        
        
    this.segImage.actor.getProperty().setGradientOpacityMinimumValue(0, 2);
    this.segImage.actor.getProperty().setGradientOpacityMinimumOpacity(0, 0.0);
    this.segImage.actor.getProperty().setGradientOpacityMaximumValue(0, 20);
    this.segImage.actor.getProperty().setGradientOpacityMaximumOpacity(0, 1.0);
    this.segImage.actor.getProperty().setShade(true);
    this.segImage.actor.getProperty().setAmbient(0.2);
    this.segImage.actor.getProperty().setDiffuse(0.2);
    this.segImage.actor.getProperty().setSpecular(0.3);
    this.segImage.actor.getProperty().setSpecularPower(8.0);
    
    this.scene.renderWindow.render();
  }

  componentDidMount() {
    if(!this.props.imageDataSet.getImage(this.props.SeriesInstanceUID))
      return;
    
    if(this.props.secondarySeriesInstanceUID && !this.props.imageDataSet.getImage(this.props.secondarySeriesInstanceUID))
      return;
    
    this.scene.genericRenderWindow = vtkGenericRenderWindow.newInstance({
      background: [0, 0, 0],
    });

    this.scene.genericRenderWindow.setContainer(this.scene.container.current);
    this.scene.renderer = this.scene.genericRenderWindow.getRenderer();
    this.scene.renderWindow = this.scene.genericRenderWindow.getRenderWindow();

    this.image = {
      data: null,
      imageMapper: vtkVolumeMapper.newInstance(),
      actor: vtkVolume.newInstance(),
      cfun: vtkColorTransferFunction.newInstance(),
      ofun: vtkPiecewiseFunction.newInstance(),
    };
    
    this.image.actor.setMapper(this.image.imageMapper);
    
    const image = this.props.imageDataSet.getImage(this.props.SeriesInstanceUID);
    const data = image.getVTKData();
    this.image.data = data;
    
    this.image.imageProperties = image.getImageProperties();
    this.image.imageProperties.addListener(this.imagePropertiesListener);
    
    this.image.imageMapper.setInputData(data);
    
    this.image.imageMapper.setMaximumSamplesPerRay(2000);
    
    this.applyPreset(this.image.imageProperties.getTransferFunction());
    
    this.activeSeriesInstanceUID = this.props.SeriesInstanceUID; // To avoid component from being unecessarily remounted
    
    this.scene.renderer.addViewProp(this.image.actor);

    if (this.props.segDataSet && this.props.segDataSet.getVTKData()) {
      this.segImage = {
        data: null,
        imageMapper: vtkVolumeMapper.newInstance(),
        actor: vtkVolume.newInstance(),
        cfun: vtkColorTransferFunction.newInstance(),
        ofun: vtkPiecewiseFunction.newInstance(),
      };
      
      this.segImage.actor.setMapper(this.segImage.imageMapper);
    
      const segData = this.props.segDataSet.getVTKData();
      this.segImage.data = segData;
      
      this.segImage.imageMapper.setInputData(segData);
      
      const segs = this.state.segDataSet.getSegs();
      
      segs.forEach((seg, index) => {
        this.listenerIds.push(seg.addListener((whatChange) => {this.onSegChange(index, whatChange)}));
      });
      
      this.setLabelsColor();
      
      this.segImage.imageMapper.setMaximumSamplesPerRay(2000);
      
      this.scene.renderer.addViewProp(this.segImage.actor);
    }
    
    const interactor = this.scene.renderWindow.getInteractor();
    //interactor.setDesiredUpdateRate(15.0);
    interactor.setLightFollowCamera(true);
    
    this.scene.renderer.resetCamera();
    //this.scene.renderer.getActiveCamera().zoom(1.5);
    //this.scene.renderer.getActiveCamera().elevation(1000);
    this.scene.renderer.getActiveCamera().setParallelProjection(true);
    this.scene.renderer.updateLightsGeometryToFollowCamera();
    
    if (!image.getTrueDirectionDiffers()) {
      this.reader = vtkSTLReader.newInstance();
      this.orientationMapper = vtkMapper.newInstance({ scalarVisibility: false });
      this.orientationActor = vtkActor.newInstance();
      
      this.orientationActor.setMapper(this.orientationMapper);
      this.orientationMapper.setInputConnection(this.reader.getOutputPort());

      this.reader.setUrl(`${human_standing}`, { binary: true }).then(() => {
          this.orientationWidget = vtkOrientationMarkerWidget.newInstance({
            actor: this.orientationActor,
            interactor: this.scene.renderWindow.getInteractor(),
          });        
          
          this.orientationWidget.setEnabled(true);
          this.orientationWidget.setViewportCorner(
            vtkOrientationMarkerWidget.Corners.BOTTOM_RIGHT
          );
          this.orientationWidget.setViewportSize(0.2);
          this.orientationWidget.setMinPixelSize(100);
          this.orientationWidget.setMaxPixelSize(300);
      });
    }
    
    this.scene.renderWindow.render();

    // TODO: Not sure why this is necessary to force the initial draw
    this.scene.genericRenderWindow.resize();
  }
  
  componentWillUnmount() {
    if(this.state.segDataSet) {
      const segs = this.state.segDataSet.getSegs();
     
      segs.forEach((seg, index) => {
        this.listenerIds.forEach(id => {
          seg.removeListener(id);
        });
      });
    }
    
    this.image.imageMapper.delete();
    this.image.imageMapper = null;
    
    this.image.actor.delete();
    this.image.actor = null;
    
    this.image.imageProperties.removeListener(this.imagePropertiesListener);
    
    if (this.orientationMapper) {
      this.orientationMapper.delete();
      this.orientationActor.delete();
      //this.orientationWidget.delete();
      this.reader.delete();
      
      this.orientationActor = null;
      this.orientationMapper = null;
      this.orientationWidget = null;
      this.reader = null;
    }
    
    if (this.segImage) {
      this.segImage.imageMapper.delete();
      this.segImage.actor.delete();
      this.segImage = null;
    }
    
    if(this.scene.genericRenderWindow) {
      //this.scene.genericRenderWindow.setContainer(null);    
      //this.scene.genericRenderWindow.getOpenGLRenderWindow().delete();
      this.scene.genericRenderWindow.delete();
      this.scene.genericRenderWindow = null;
    }
    
    if(this.scene.renderer) {
      this.scene.renderer.delete();
      this.scene.renderer = null;
    }
    
    if(this.scene.renderWindow) {
      this.scene.renderWindow.delete();
      this.scene.renderWindow = null;
    }
  }
  
  componentDidUpdate() {
    console.log('Updating component');
    
    if (this.activeSeriesInstanceUID==this.props.SeriesInstanceUID &&
      this.activeSecondarySeriesInstanceUID==this.props.secondarySeriesInstanceUID)
      return;
      
    this.componentWillUnmount();
    
    this.listenerIds = [];
    
    this.componentDidMount();
  }

  render() {
    const style = { width: '100%', height: '100%', position: 'relative' };

    return (
      <div style={style}>
        <div ref={this.scene.container} style={style} />
      </div>
    );
  }
}
