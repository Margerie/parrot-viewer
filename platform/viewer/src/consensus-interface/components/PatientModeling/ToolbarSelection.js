
import React, { Component } from "react";

import ROITools from "./ROITools.js";
import FusionTools from "./FusionTools.js";

import ViewsApi from './Services/ViewsApi'
import ContourModification from './ContourModification.js';
import DataSet from './../../data/DataSet.js';
import History from './PaintFilter/history.js';

import './styles.css';

class TabLinks extends React.Component {
  render() {
    return (<button className={this.props.active ? "tablinks" + " active" : "tablinks"} id={this.props.id} onClick={() => {this.props.onClick()}}> {this.props.value} </button>);
  }
}

export default class ToolBarSelection extends React.Component {
  constructor(props) {
    super(props);
    
    this.api = {
      adjustPrimarySecondary: (value) => this.adjustPrimarySecondary(value),
      cancelModification: () => this.cancelModification(),
      endAdjust: () => this.endAdjust(),
      endAll: () => this.endAll(),
      endBrush: () => this.endBrush(),
      endCrossHairs: () => this.endCrossHairs(),
      endErase: () => this.endErase(),
      endModification: () => this.endModification(),
      endPan: () => this.endPan(),
      endSculpt: () => this.endSculpt(),
      endZoom: () => this.endZoom(),
      redo: () => this.redo(),
      setCurrentColorID: (id) => this.setCurrentColorID(id),
      setLayout: (layout) => this.setLayout(layout),
      startAdjust: () => this.startAdjust(),
      startAdjustSecondary: () => this.startAdjustSecondary(),
      startBrush: () => this.startBrush(),
      startCrossHairs: () => this.startCrossHairs(),
      startErase: () => this.startErase(),
      startModification: () => this.startModification(),
      startPan: () => this.startPan(),
      startSculpt: () => this.startSculpt(),
      startZoom: () => this.startZoom(),
      undo: () => this.undo(),
      
    };
    
    this.dataSet = DataSet.getInstance();
	  this.contourModification = ContourModification.getInstance();
	  this.history = History.getInstance();
	  this.radiusListener = (event) => this.brushRadiusListener(event);
	  this.viewApi = ViewsApi.getInstance();
	 
	  this.modificationStartListener = ()=>{this.setState({modifying:true})};
	  this.modificationEndListener = ()=>{
	    switch(this.state.activeTool) {
	      case "brush":
	        this.endBrush();
	        break;
	      case "sculpt":
	        this.endSculpt();
	        break;
	      case "erase":
	        this.endErase();
	        break;
	    }

      this.history.resetHistory()
	    
	    this.setState({modifying:false}
	  )};
	  
    this.contourModification.onModicationStart(this.modificationStartListener);
    this.contourModification.onModicationEnd(this.modificationEndListener);
	    
    this.state = {
      activeTabID: "ROITools",
      currentColorID: "None",
      activeTool: "crossHairs",
      modifying: false,
    };
    
    this.viewApi.setActiveWidget("crossHairs"); // CrossHairs are launched directly in view2D
  }
  
  
  /* TOOLS *
   *********/
  adjustPrimarySecondary(value){
     this.viewApi.adjustPrimarySecondary(value);
  }
  
  brushRadiusListener(event) {
    if (this.state.modifying) {
      switch (event.key) {
        case "+":
          this.viewApi.setPaintRadius(this.viewApi.getRadius()+1)
          break;
        case "-":
          this.viewApi.setPaintRadius(this.viewApi.getRadius()-1)
          break;
        case "z":
          if (event.ctrlKey)
            this.undo();
          break;
        case "y":
          if (event.ctrlKey)
            this.redo();
          break;
        default:
          return;
      }
    }
  }
  
  cancelModification() {
    this.history.undoAll();
    this.contourModification.cancel();
  }
  
  endAdjust() {
    this.viewApi.endAdjust();
  }
  
  endAll() {    
    switch(this.state.activeTool) {
      case "brush":
        this.endBrush();
        return;
      case "sculpt":
        this.endSculpt();
        return;
      case "erase":
        this.endErase();
        return;
      case "crossHairs":
        this.endCrossHairs();
        break;
      case "adjust":
        this.endAdjust();
        return;
      case "pan":
        this.endPan();
        return;
      case "zoom":
        this.endZoom();
        return;
      case "label":
        this.endLabel();
        break;
    }
  
    this.setState({activeTool: null});
  }
  
  endBrush() {
    this.viewApi.endBrush();
    
    this.viewApi.setActiveWidget(null);
    this.setState({activeTool: this.viewApi.getActiveWidget()});
  }
  
  endCrossHairs() {
    this.viewApi.endCrossHairs();
    
    this.viewApi.setActiveWidget(null);
    this.setState({activeTool: this.viewApi.getActiveWidget()});
  }
  
  endErase() {
    this.viewApi.endErase();
    
    this.viewApi.setActiveWidget(null);
    this.setState({activeTool: this.viewApi.getActiveWidget()});
  }

  endLabel() {
    this.viewApi.endLabel();
  }
  
  endModification() {
    this.endAll();
    this.setState({modifying: false})
  }
  
  endPan() {
    this.viewApi.endPan();
  }
  
  endSculpt() {
    this.viewApi.endSculpt();
    
    this.viewApi.setActiveWidget(null);
    this.setState({activeTool: this.viewApi.getActiveWidget()});
  }
  
  endZoom() {
    this.viewApi.endZoom();
  }
  
  redo() {
    this.history.redo();
  }
  
  setCurrentColorID(currentColorID) {
    this.setState({currentColorID});
  }
  
  setLayout(layout) {
    this.props.setLayout(layout);
  }
  
  startAdjust() {
    if (this.state.activeTool=="adjust") {
      this.startCrossHairs();
      return;
    }
    
    if(this.viewApi.startAdjust()) {
      this.viewApi.setActiveWidget("adjust");
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startAdjustSecondary() {
    if (this.state.activeTool=="adjustSecondary") {
      this.startCrossHairs();
      return;
    }
    
    if(this.viewApi.startAdjustSecondary()) {
      this.viewApi.setActiveWidget("adjustSecondary");
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startBrush() {
    if (this.state.activeTool=="brush") {
      this.startCrossHairs();
      return;
    } else
      this.endAll();
    
    if(this.state.modifying && this.viewApi.startBrush()) {
      this.viewApi.setActiveWidget("brush");
      
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startCrossHairs() {
    if (this.state.activeTool=="crossHairs") {
      this.endCrossHairs();
      return;
    } else
      this.endAll();
    
	  if(this.viewApi.startCrossHairs()) {
      this.viewApi.setActiveWidget("crossHairs");
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startErase() {
    if (this.state.activeTool=="erase") {
      this.startCrossHairs();
      return;
    } else
      this.endAll();
    
    if(this.state.modifying && this.viewApi.startErase()) {
      this.viewApi.setActiveWidget("erase");
      
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startLabel() {
    if(this.viewApi.startLabel())
      this.setState({activeTool: "label"});
  }
  
  startModification() {
    this.setState({modifying: true});
  }
  
  startPan() {
    if (this.state.activeTool=="pan") {
      this.startCrossHairs();
      return;
    } else
      this.endAll();
    
    if(this.viewApi.startPan()) {
      this.viewApi.setActiveWidget("pan");
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startSculpt() {
    if (this.state.activeTool=="sculpt") {
      this.startCrossHairs();
      return;
    } else
      this.endAll();
    
    if(this.state.modifying && this.viewApi.startSculpt()) {
      this.viewApi.setActiveWidget("sculpt");
      
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  startZoom() {
    if (this.state.activeTool=="zoom") {
      this.startCrossHairs();
      return;
    } else
      this.endAll();
    
    if(this.viewApi.startZoom()) {
      this.viewApi.setActiveWidget("zoom");
      this.setState({activeTool: this.viewApi.getActiveWidget()});
    }
  }
  
  undo() {
    this.history.undo();
  }
  
  
  /* MAIN *
   ********/
  
  componentDidMount() {
    window.addEventListener("keydown", this.radiusListener);
  }
  
  componentWillUnmount() {
    this.endAll();
    this.cancelModification()
    
    window.removeEventListener("keydown", this.radiusListener);
    
    this.contourModification.removeModificationStartListener(this.modificationStartListener);
    this.contourModification.removeModificationEndListener(this.modificationEndListener);
  }
  
  handleTabLinksClick(activeTabID) {
    this.setState({activeTabID});
  }
  
  activeTabContent() {    
    switch (this.state.activeTabID) {
      case "ROITools":
        return (<ROITools 
          layout={this.props.layout}
          activeTool={this.state.activeTool}
          modifying={this.state.modifying}
          api={this.api}
        />);
      case "FusionTools":
        return (<FusionTools
          layout={this.props.layout}
          activeTool={this.state.activeTool}
          modifying={this.state.modifying}
          currentColorID = {this.state.currentColorID}
          SeriesInstanceUID={this.props.SeriesInstanceUID}
          secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
          imageDataSet={this.props.imageDataSet}
          api={this.api}
        />);
      default:
        return (<div> Error </div>);
    }
  }
  
  render() {
    const activeTabID = this.state.activeTabID;
    const tabContent = this.activeTabContent();
    
    return (
      <div className="ToolBarSelection">
        <div className="toolsTab">
          <TabLinks id={"ROITools"} active={activeTabID==="ROITools"} value={"ROI Tools"} onClick={() => {this.handleTabLinksClick("ROITools")}}/>
          <TabLinks id={"FusionTools"} active={activeTabID==="FusionTools"} value={"Fusion"} onClick={() => {this.handleTabLinksClick("FusionTools")}}/>
        </div>
        <div className="toolsTabContent">
          {tabContent}
        </div>
      </div>
    );
  }
}

