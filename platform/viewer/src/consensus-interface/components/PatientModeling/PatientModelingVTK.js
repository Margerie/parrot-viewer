import React from 'react';
import uniqBy from 'lodash/uniqBy';

import VTKViews from './VTKViews.js';
// import VTKView from './VTKView.js';
import ImagePreviews from './ImagePreviews.js';
import ContourBrowser from './ContourBrowser.js';
import SegSelection from './SegSelection.js';
import ToolbarSelection from './ToolbarSelection.js';
import DataSet from './../../data/DataSet.js';
import ViewsApi from './Services/ViewsApi';
import { Icon } from '../../utils/icons/Icon';
import HistoryBrowser from './HistoryBrowser.js';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import './styles.css';

export default class PatientModeling extends React.Component {
  constructor(props) {
    super(props);

    this.dataSet = DataSet.getInstance();

    this.dataListener = (event) => this.dataSetListener(event);
    this.dataSet.addListener(this.dataListener);

    this.imageApi = null;

    this.promiseSeriesInstanceUID = null;
    this.promiseSecondarySeriesInstanceUID = null;
    this.promiseSEGSeriesUID = null;
    this.promiseSEGSOPUID = null;

    this.state = {
      activeSeriesInstanceUID: null,
      activeSecondarySeriesInstanceUID: null,
      activeSEGSeriesUID: null,
      activeSEGSOPUID: null,
      imageDataSet: this.dataSet.getImageDataSet(),
      segs: null,
      doseDataSet: this.dataSet.getDoseDataSet(),
      contourModificationHistoryDataSet: this.dataSet.getContourModificationHistoryDataSet(),
      StudyInstanceUID: this.props.activeStudyInstanceUID,
      study: this.dataSet.getStudy(),
      historyOn: true,
      layout: 3,
      historySelection: {},
    };
  }

  dataSetListener(event) {
    if (event.type == 'created') {
      // In case not already loaded when calling constructor
      switch (event.target) {
        case this.props.activeStudyInstanceUID:
          this.setState({ study: this.dataSet.getStudy() });
          break;
        case this.promiseSeriesInstanceUID:
          break;
        case this.promiseSecondarySeriesInstanceUID:
          break;
        case this.promiseSEGSOPUID:
          const segs = this.dataSet.getSegDataSet().getSegs(this.promiseSEGSOPUID);
          this.setState({
            segs: segs,
            activeSEGSeriesUID: this.promiseSEGSeriesUID,
            activeSEGSOPUID: this.promiseSEGSOPUID,
            historySelection: this.computeHistorySelection(segs),
          });
          break;
      }
    }
  }

  componentWillUnmount() {
    this.dataSet.removeListener(this.dataListener);
  }

  getSeriesInstanceUID(st) {
    if (!st) return null;

    const allSeries = uniqBy(st, 'SeriesInstanceUID');
    if (!allSeries.length) return null;

    const CTUIds = allSeries.filter((study) => study.modalities == 'CT').map((study) => study.SeriesInstanceUID);

    let CTUId;
    if (CTUIds.length) CTUId = CTUIds[0];
    else CTUId = allSeries.filter((study) => study.modalities == 'MR' || study.modalities == 'PT').map((study) => study.SeriesInstanceUID)[0];

    return CTUId;
  }

  getSeriesInstanceUIDs() {
    if (!this.state.study) return null;

    return uniqBy(this.state.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'CT' || study.modalities == 'MR' || study.modalities == 'PT')
      .map((study) => study.SeriesInstanceUID);
  }

  handleActiveSeriesInstanceUIDSelected(activeSeriesInstanceUID) {
    const imageDataSet = this.dataSet.getImageDataSet();
    if (imageDataSet && imageDataSet.getImage(activeSeriesInstanceUID)) {
      this.setState({ imageDataSet, activeSeriesInstanceUID });

      if (this.imageApi) ViewsApi.getInstance().unregisterApi(this.imageApi);

      this.imageApi = imageDataSet.getImage(activeSeriesInstanceUID).getImageProperties().getApi();

      ViewsApi.getInstance().registerApi(this.imageApi);
    }
  }

  handleActiveSecondarySeriesInstanceUIDSelected(activeSecondarySeriesInstanceUID) {
    let layout = this.state.layout;

    if (!activeSecondarySeriesInstanceUID) layout = 3;

    this.setState({ layout, activeSecondarySeriesInstanceUID });
  }

  handlesSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID) {
    this.promiseSEGSOPUID = activeSEGSOPUID;

    const segs =
      activeSEGSOPUID == -1
        ? {
            getSegs: () =>
              this.dataSet
                .getSegDataSet()
                .getAllSegs()
                .map((s) => s.getSegs())
                .flat(),
          }
        : this.dataSet.getSegDataSet().getSegs(activeSEGSOPUID);

    const historySelection = this.computeHistorySelection(segs);
    if (segs) this.setState({ segs, activeSEGSeriesUID, activeSEGSOPUID, historySelection });
  }

  setUserPosition(userPosition) {
    if (this.state.imageDataSet) this.state.imageDataSet.getImages().forEach((image) => image.getImageProperties().setUserPosition(userPosition));
  }

  setLayout(layout) {
    if (layout == this.state.layout) return;
    if (layout == 2 && !this.state.activeSecondarySeriesInstanceUID) {
      return confirmAlert({
        title: 'Patient Modeling Layout',
        closeOnEscape: false,
        closeOnClickOutside: false,
        message: 'You must select a secondary image to use this layout.',
        buttons: [
          {
            label: 'OK',
            onClick: () => {},
          },
        ],
      });
      // return;
    }
    this.setState({ layout });
  }

  /**
   * @returns a map segmentID -> { vtkID: VTKDataVersionID, name: fullname } for all segments
   */
  computeHistorySelection(segs) {
    const historySelection = {};
    if (segs) {
      segs.getSegs().forEach((seg) => {
        historySelection[seg.getID()] = {
          vtkID: seg.getVTKDataVersionID(),
          name: seg.getFullName(),
        };
      });
    }
    return historySelection;
  }

  async selectModificationHistory(historyEntry) {
    // find the segment for this history entry
    const seg = this.state.segs.getSegs().find((seg) => seg.getID() == historyEntry.contourID);
    // load the segment data for this history entry (if not already loaded)
    if (seg && seg.getVTKDataVersionID() != historyEntry.attachmentID) {
      const { data, attachmentID } = await this.state.contourModificationHistoryDataSet.getSegmentData(
        seg,
        historyEntry.attachmentID,
        this.state.StudyInstanceUID
      );
      seg.prepareVTKData(data, attachmentID);
      // recalculate history selection
      const historySelection = this.computeHistorySelection(this.state.segs);
      this.setState({ historySelection });
    }
  }

  async deleteModificationHistory(historyEntry) {
    // delete history entry
    await this.state.contourModificationHistoryDataSet.removeHistory(historyEntry, this.state.StudyInstanceUID);
    // select latest version in history for the segment if the deleted entry was the currently selected
    const seg = this.state.segs.getSegs().find((seg) => seg.getID() == historyEntry.contourID);
    if (seg && seg.getVTKDataVersionID() == historyEntry.attachmentID) {
      const { data, attachmentID } = await this.state.contourModificationHistoryDataSet.getSegmentData(seg, -1, this.state.StudyInstanceUID);
      seg.prepareVTKData(data, attachmentID);
      // recalculate history selection
      const historySelection = this.computeHistorySelection(this.state.segs);
      this.setState({ historySelection });
    }
  }

  async addModificationHistory(modification, modifiedSegment) {
    // add history entry
    await this.state.contourModificationHistoryDataSet.addHistory(modification, modifiedSegment, this.state.StudyInstanceUID);
    if (modification.attachmentID) {
      modifiedSegment.setVTKDataVersionID(modification.attachmentID);
    }
    // recalculate history selection
    const historySelection = this.computeHistorySelection(this.state.segs);
    this.setState({ historySelection });
  }

  renderPreviews() {
    if (this.state.study)
      return (
        <ImagePreviews
          className="imagePreviews"
          clickEnabled={true}
          activeSeriesInstanceUID={this.state.activeSeriesInstanceUID}
          activeSecondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
          SeriesInstanceUIDs={this.getSeriesInstanceUIDs()}
          onPrimaryItemClick={(activeSeriesInstanceUID) => {
            this.handleActiveSeriesInstanceUIDSelected(activeSeriesInstanceUID);
          }}
          onSecondaryItemClick={(activeSeriesInstanceUID) => {
            this.handleActiveSecondarySeriesInstanceUIDSelected(activeSeriesInstanceUID);
          }}
        />
      );
    else return <h2 style={{ color: 'red', width: '400px', textAlign: 'center' }}>No study selected</h2>;
  }

  renderImageView() {
    if (this.state.activeSeriesInstanceUID) {
      if (this.state.imageDataSet && this.state.imageDataSet.getImage(this.state.activeSeriesInstanceUID)) {
        return (
          <VTKViews
            SeriesInstanceUID={this.state.activeSeriesInstanceUID}
            secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
            segDataSet={this.state.segs}
            doseDataSet={this.state.doseDataSet}
            imageDataSet={this.state.imageDataSet}
            layout={this.state.layout}
          />
        );
      } else {
        return <div>Loading...</div>;
      }
    } else return <h2 style={{ color: 'red', width: '400px', textAlign: 'center' }}>No series selected</h2>;
  }

  storeApis(api, apis) {
    this.api = api;
    this.vtkViewsApis = apis;
  }

  componentWillUnmount() {
    if (this.imageApi) ViewsApi.getInstance().unregisterApi(this.imageApi);
  }

  renderModificationHistoryView(caretIcon) {
    if (this.state.historyOn && this.state.contourModificationHistoryDataSet) {
      return (
        <div className="modiviationHistoryImageViewDiv">
          <HistoryBrowser
            contourModificationHistoryDataSet={this.state.contourModificationHistoryDataSet}
            StudyInstanceUID={this.state.StudyInstanceUID}
            historySelection={this.state.historySelection}
            onSelectHistory={(entry) => this.selectModificationHistory(entry)}
            onDeleteHistory={(entry) => this.deleteModificationHistory(entry)}
          />
          {this.renderPreviews()}
          <div className="previewsIcon">{caretIcon}</div>
        </div>
      );
    } else {
      return (
        <div>
          {this.renderPreviews()}
          {this.state.study ? <div className="previewsIcon">{caretIcon}</div> : null}
        </div>
      );
    }
  }

  render() {
    const imageView = this.renderImageView();

    let segPanel = <div></div>;

    if (this.state.study) {
      let cBrowser = null;
      if (!this.state.segs) {
        segPanel = <div className="SegPanelContent">Loading...</div>;
      } else {
        cBrowser = (
          <ContourBrowser
            className="contourBrowser"
            segDataSet={this.state.segs}
            contourModificationHistoryDataSet={this.state.contourModificationHistoryDataSet}
            showEdit={true}
            setUserPosition={(userPosition) => this.setUserPosition(userPosition)}
            isPlanEvaluation={false}
            StudyInstanceUID={this.state.StudyInstanceUID}
            onAddHistory={(modification, modifiedSegment) => this.addModificationHistory(modification, modifiedSegment)}
          />
        );
      }

      segPanel = (
        <div className="SegPanelContent">
          <SegSelection
            study={this.state.study}
            onSelectedSEG={(activeSEGSeriesUID, activeSEGSOPUID) => {
              this.handlesSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID);
            }}
          />
          {cBrowser}
        </div>
      );
    } else if (this.state.StudyInstanceUID) {
      segPanel = <div>Loading...</div>;
    }

    let caretStyle = {
      width: '100%',
      height: '20px',
      verticalAlign: 'center',
      textAlign: 'center',
    };
    let iconName = 'caret-down';
    if (this.state.historyOn) iconName = 'caret-up';

    let caretIcon = (
      <Icon
        style={caretStyle}
        name={iconName}
        onClick={() =>
          this.setState({
            historyOn: !this.state.historyOn,
          })
        }
      />
    );

    return (
      <div className="PatientModelingVTK">
        <div className="PatientModelingVTKToolbar">
          <ToolbarSelection
            layout={this.state.layout}
            setLayout={(layout) => this.setLayout(layout)}
            SeriesInstanceUID={this.state.activeSeriesInstanceUID}
            secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
            imageDataSet={this.state.imageDataSet}
          />
        </div>
        <div className="PatientModelingVTKMain">
          <div className="SegPanel">{segPanel}</div>
          <div className="ImageViews">
            <div className={this.state.historyOn ? 'ModelingImageView' : 'ModelingImageView extended'}>{imageView}</div>
            {this.renderModificationHistoryView(caretIcon)}
          </div>
        </div>
      </div>
    );
  }
}
