import React, { Component } from 'react';

import ViewsApi from '../Services/ViewsApi.js';
import DataSet from '../../../data/DataSet.js';
import { Icon } from '../../../utils/icons/Icon';

import '../PlanEvaluation/style/styles.css';

export default class DoseToolBar extends React.Component {
  constructor(props) {
    super(props);

    this.api = {
      adjustPrimarySecondary: (value) => this.adjustPrimarySecondary(value),
      endAll: () => this.endAll(),
      endCrossHairs: () => this.endCrossHairs(),
      startCrossHairs: () => this.startCrossHairs(),
      startAdjust: () => this.startAdjust(),
      endAdjust: () => this.endAdjust(),
      startAdjustSecondary: () => this.startAdjustSecondary(),
    };

    this.dataSet = DataSet.getInstance();
    this.viewApi = ViewsApi.getInstance();

    this.state = {
      currentColorID: 'None',
      activeTool: 'crossHairs',
      settingWindow: null,
    };

    this.viewApi.setActiveWidget('crossHairs'); // CrossHairs are launched directly in view2D
  }

  /* TOOLS *
   *********/
  closeSettingWindow() {
    this.setState({ settingWindow: null });
  }

  adjustPrimarySecondary(value) {
    this.viewApi.adjustPrimarySecondary(value);
  }

  endAll() {
    switch (this.state.activeTool) {
      case 'adjust':
        this.endAdjust();
        return;
      case 'crossHairs':
        this.endCrossHairs();
        break;
      default:
    }
    this.setState({ activeTool: null });
  }

  endCrossHairs() {
    this.viewApi.endCrossHairs();

    this.viewApi.setActiveWidget(null);
    this.setState({ activeTool: this.viewApi.getActiveWidget() });
  }

  startCrossHairs() {
    if (this.state.activeTool === 'crossHairs') {
      this.endCrossHairs();
      return;
    } else this.endAll();

    if (this.viewApi.startCrossHairs()) {
      this.viewApi.setActiveWidget('crossHairs');
      this.setState({
        activeTool: this.viewApi.getActiveWidget(),
      });
    }
  }

  endAdjust() {
    this.viewApi.endAdjust();
  }

  startAdjust() {
    if (this.state.activeTool === 'adjust') {
      this.startCrossHairs();
      return;
    }

    if (this.viewApi.startAdjust()) {
      this.viewApi.setActiveWidget('adjust');
      this.setState({ activeTool: this.viewApi.getActiveWidget() });
    }
  }

  startAdjustSecondary() {
    if (this.state.activeTool === 'adjustSecondary') {
      this.startCrossHairs();
      return;
    }

    if (this.viewApi.startAdjustSecondary()) {
      this.viewApi.setActiveWidget('adjustSecondary');
      this.setState({ activeTool: this.viewApi.getActiveWidget() });
    }
  }

  toggleSettingsWindow() {
    let windowSetting = null;
    switch (this.props.activeTool) {
      case 'crossHairs':
        break;
      case 'adjust':
        windowSetting = <WindowWLSettings onClose={() => this.closeSettingWindow()} />;
        break;
      default:
        windowSetting = null;
        break;
    }
    if (windowSetting !== null) {
      this.setState({ settingWindow: windowSetting });
    }
  }

  /* MAIN *
   ********/
  activeTabContent() {
    return <DoseTools api={this.api} />;
  }

  render() {
    let optionLabel = null;
    let optionMenu = null;
    let iconName = null;
    switch (this.props.activeTool) {
      case 'crosshairs':
        break;
      case 'adjust':
        optionLabel = 'W W/L';
        iconName = 'level';
        break;
      default:
    }
    if (optionLabel) {
      optionMenu = (
        <table>
          <tr onClick={() => this.toggleSettingsWindow()}>
            <td>
              <Icon className="medIcon" name={iconName}></Icon>
            </td>
            <td>{optionLabel + ' options'}</td>
          </tr>
        </table>
      );
    }

    return (
      <div className="DoseToolBar">
        <table className="doseToolTable">
          <tr className="crossHarisTr" onClick={() => this.startCrossHairs()}>
            <td>
              <Icon className="smallIcon" name="crosshairs" />
            </td>
            <td className="crossHarisTd">Crosshairs</td>
          </tr>
        </table>
        {this.state.settingWindow}
      </div>
    );
  }
}
