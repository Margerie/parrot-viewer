import React from 'react';

import VTKView from '../VTKView.js';
import '../PlanEvaluation/style/styles.css';

import DoseColorBar, { calculateColorColumnData } from './DoseColorBar';

export default class VTKDoseViews extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeLabel: '1',
      displayCrosshairs: true,
      secondarySeriesInstanceUID: this.props.secondarySeriesInstanceUID,
      SeriesInstanceUID: this.props.SeriesInstanceUID,
      sliceModel: 'K',
      doseColorSettings: {
        referenceValue: 70.0,
        maxDoseValue: 70.0,
        doseData: calculateColorColumnData(70.0, 'Dose', null, null),
        doseDiffData: calculateColorColumnData(0.0, 'DoseDiff', null, null),
      },
    };

    this.colorbarFunctions = {};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.secondarySeriesInstanceUID != nextProps.secondarySeriesInstanceUID ||
      this.props.SeriesInstanceUID != nextProps.SeriesInstanceUID ||
      this.props.doseMode != nextProps.doseMode ||
      this.props.doseScaleFactor != nextProps.doseScaleFactor ||
      JSON.stringify(this.state.doseColorSettings) !== JSON.stringify(nextState.doseColorSettings) ||
      this.props.segDataSet != nextProps.segDataSet ||
      this.props.doseIndex1 !== nextProps.doseIndex1 ||
      this.props.doseIndex2 !== nextProps.doseIndex2
    );
  }

  setDoseColor() {
    const doses = this.props.doseDataSet?.getDoses();
    if (this.props.doseIndex1 !== null && this.props.doseIndex2 !== null) {
      doses[this.props.doseIndex1].setDoseColor([255, 255, 0]);
      doses[this.props.doseIndex2].setDoseColor([0, 0, 255]);
    } else if (this.props.doseIndex1 !== null || this.props.doseIndex2 !== null) {
      doses[this.props.doseIndex1 !== null ? this.props.doseIndex1 : this.props.doseIndex2].setDoseColor([255, 255, 0]);
    }
  }

  renderDoseView(divClassName, sliceMode, viewType, title, doseToShow) {
    return (
      <div className={divClassName}>
        <VTKView
          SeriesInstanceUID={this.props.SeriesInstanceUID}
          secondarySeriesInstanceUID={this.props.secondarySeriesInstanceUID}
          fullScreen={this.state.fullScreen}
          imageDataSet={this.props.imageDataSet}
          segDataSet={this.props.segDataSet}
          doseDataSet={this.props.doseDataSet}
          doseIndex1={this.props.doseIndex1}
          doseIndex2={this.props.doseIndex2}
          sliceMode={sliceMode}
          setFullScreen={(sliceMode, value) => {
            this.setFullScreen(sliceMode, value);
          }}
          viewType={viewType}
          title={this.props.doseIndex1 !== null || this.props.doseIndex2 !== null ? title : ''}
          doseToShow={doseToShow}
          doseColorSettings={this.state.doseColorSettings}
          doseScaleFactor={this.props.doseScaleFactor}
          colorbarFunctions={this.colorbarFunctions}
          show3D={'No'}
        />
      </div>
    );
  }

  showDoseImageByDoseMode() {
    if (this.props.doseMode == 2) {
      return (
        <div className="dose-col-imagePreview">
          {this.renderDoseView('dose-row-sm-1', 'K', '2D', 'Dose Difference', 2)}
          <div className="dose-row-sm-2">
            {this.renderDoseView('dose-row-sm-2-col', 'I', '2D', 'Dose 1', 0)}
            {this.renderDoseView('dose-row-sm-2-col', 'J', '2D', 'Dose 2', 1)}
          </div>
        </div>
      );
    } else if (this.props.doseMode == 0) {
      return (
        <div className="dose-col-imagePreview">
          {this.renderDoseView('dose-row-sm-1', 'K', '2D', 'Dose 1', 0)}
          <div className="dose-row-sm-2">
            {this.renderDoseView('dose-row-sm-2-col', 'I', '2D', 'Dose 1', 0)}
            {this.renderDoseView('dose-row-sm-2-col', 'J', '2D', 'Dose 1', 0)}
          </div>
        </div>
      );
    } else if (this.props.doseMode == 1) {
      return (
        <div className="dose-col-imagePreview">
          {this.renderDoseView('dose-row-sm-1', 'K', '2D', 'Dose 2', 1)}
          <div className="dose-row-sm-2">
            {this.renderDoseView('dose-row-sm-2-col', 'I', '2D', 'Dose 2', 1)}
            {this.renderDoseView('dose-row-sm-2-col', 'J', '2D', 'Dose 2', 1)}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }

  render() {
    if (!this.state.SeriesInstanceUID) {
      return null;
    }
    this.setDoseColor();
    return (
      <div className="dose-View3">
        {this.showDoseImageByDoseMode()}
        <DoseColorBar
          className="doseColorBar"
          colorbarFunctions={this.colorbarFunctions}
          doseColorSettings={this.state.doseColorSettings}
          onColorsChanged={(newDoseColorSettings) => {
            this.setState({
              doseColorSettings: newDoseColorSettings,
            });
          }}
        />
      </div>
    );
  }
}
