import { Button, Modal } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';
import '../style/clinicalGoalDialogStyle.css';
var XLSX = require('xlsx');

class DragDropFile extends React.Component {
  constructor(props) {
    super(props);
    this.onDrop = this.onDrop.bind(this);

    DragDropFile.propTypes = {
      files: PropTypes.arrayOf(PropTypes.object),
      handleFile: PropTypes.object,
      children: PropTypes.arrayOf(PropTypes.object),
    };
  }

  suppress(evt) {
    evt.stopPropagation();
    evt.preventDefault();
  }

  onDrop(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    const files = evt.dataTransfer.files;
    if (files && files[0]) {
      this.props.handleFile(files[0]);
    }
  }
  render() {
    return (
      <div onDrop={this.onDrop} onDragEnter={this.suppress} onDragOver={this.suppress}>
        {this.props.children}
      </div>
    );
  }
}

class DataInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);

    DataInput.propTypes = {
      handleFile: PropTypes.func,
    };
  }

  handleChange(e) {
    const files = e.target.files;
    if (files && files[0]) {
      this.props.handleFile(files[0]);
    }
  }

  render() {
    return (
      <div className="cgLoadingLabelInputDiv">
        <div className="cgLoadingLabelDiv">
          <h3 className="cgSelectFileLabel" htmlFor="file">
            Select the file containing the clinical goals to import
          </h3>
        </div>
        <div className="cgLoadingInputDiv">
          <input type="file" className="form-control" id="file" accept={SheetJSFT} onChange={this.handleChange}></input>
        </div>
      </div>
    );
  }
}

class OutTable extends React.Component {
  excelDateToJSDate(serial) {
    var utc_days = Math.floor(serial - 25569);
    var utc_value = utc_days * 86400;
    var date_info = new Date(utc_value * 1000);

    var fractional_day = serial - Math.floor(serial) + 0.0000001;

    var total_seconds = Math.floor(86400 * fractional_day);

    var seconds = total_seconds % 60;

    total_seconds -= seconds;

    var hours = Math.floor(total_seconds / (60 * 60));
    var minutes = Math.floor(total_seconds / 60) % 60;

    return new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
  }

  renderOutTableData(row, cols) {
    let key = '';
    let keyUC = '';
    let value = null;

    if (cols[0] && cols[2]) {
      key = row[cols[0].key];
      keyUC = key.toUpperCase();
      value = row[cols[2].key];
    } else {
      return <h4 className="cg-format-message">Did you select the correct file?</h4>;
    }

    if (keyUC.includes('NAME')) {
      return (
        <tr className="protocolNameTR">
          <td>{key}:</td>
          <td className="protocolName">{value}</td>
        </tr>
      );
    } else if (keyUC.includes('CREATED')) {
      return (
        <tr className="createdOnTR">
          <td>{key}:</td>
          <td className="createdOn">{this.excelDateToJSDate(value).toLocaleDateString()}</td>
        </tr>
      );
    } else if (keyUC.includes('COMMENT')) {
      return (
        <tr className="commentTR">
          <td>{key}:</td>
          <td className="comment">{value}</td>
        </tr>
      );
    } else {
      return null;
    }
  }

  render() {
    return (
      <div className="cg-table-responsive">
        <table className="cg-table-striped">
          <thead></thead>
          <tbody className="cgOutputTableTbody">{this.props.data.slice(0, 3).map((r, i) => this.renderOutTableData(r, this.props.cols))}</tbody>
        </table>
      </div>
    );
  }
}

OutTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  cols: PropTypes.arrayOf(PropTypes.object),
};

/* list of supported file types */
const SheetJSFT = ['xlsx', 'xlsb', 'xlsm', 'xls', 'csv', 'txt']
  .map(function (x) {
    return '.' + x;
  })
  .join(',');

/** generate an array of column objects */
const make_cols = (refstr) => {
  let o = [],
    C = XLSX.utils.decode_range(refstr).e.c + 1;
  for (var i = 0; i < C; ++i) {
    o[i] = { name: XLSX.utils.encode_col(i), key: i };
  }
  return o;
};

export default class ClinicalGoalsLoadParseDialog extends React.Component {
  constructor(props) {
    super(props);

    ClinicalGoalsLoadParseDialog.propTypes = {
      clinicalGoalsLoadParseFunction: PropTypes.func,
      enterPrescriptionValue: PropTypes.number,
      onGoalsFileLoaded: PropTypes.func,
      setClinicalGoalFileName: PropTypes.func,
    };

    props.clinicalGoalsLoadParseFunction.setVisible = (isVisible) => this.setVisible(isVisible);
    this.state = {
      visible: false,
      enterPrescriptionValue: props.enterPrescriptionValue,
      selectedCgFileName: null,
      data: [],
      cols: [],
    };
    this.handleFile = this.handleFile.bind(this);
  }

  componentDidUpdate(prevProps) {
    this.props.clinicalGoalsLoadParseFunction.setVisible = (isVisible) => this.setVisible(isVisible);
  }

  handleFile(file) {
    const reader = new FileReader();
    if (file) {
      this.setState({ selectedCgFileName: file.name });
    }

    const rABS = !!reader.readAsBinaryString;
    reader.onload = (e) => {
      /* Parse data */
      const bstr = e.target.result;
      const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array' });

      /* Get first worksheet */
      const wsname = wb.SheetNames[0];
      const ws = wb.Sheets[wsname];
      // console.log(rABS, wb);

      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_json(ws, { header: 1 });

      /* Update state */
      this.setState({ data: data, cols: make_cols(ws['!ref']) });
    };
    if (rABS) {
      reader.readAsBinaryString(file);
    } else {
      reader.readAsArrayBuffer(file);
    }
  }

  setVisible(isVisible) {
    if (isVisible) {
      this.setState({
        cols: [],
        data: [],
      });
    }
    this.setState({
      visible: isVisible,
    });
  }

  handleOK() {
    this.setVisible(false);
    this.props.setClinicalGoalFileName(this.state.selectedCgFileName);
    this.props.onGoalsFileLoaded(this.state.data);
  }

  handleCancel() {
    this.setVisible(false);
  }

  renderLoadingClinicalGoalsFile() {
    return (
      <DragDropFile handleFile={this.handleFile}>
        <div className="row">
          <div className="col-xs-12">
            <DataInput handleFile={this.handleFile} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <OutTable className="cgOutputTableInDialog" data={this.state.data} cols={this.state.cols} />
          </div>
        </div>
      </DragDropFile>
    );
  }

  render() {
    // console.log("************ render clinical goals");
    return (
      <Modal
        className="clinicalGoalsModal"
        destroyOnClose={true}
        open={this.state.visible}
        onOk={() => this.handleOK()}
        onCancel={() => this.handleCancel()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="ok" type="primary" onClick={() => this.handleOK()}>
            OK
          </Button>,
        ]}
      >
        {this.renderLoadingClinicalGoalsFile()}
      </Modal>
    );
  }
}
