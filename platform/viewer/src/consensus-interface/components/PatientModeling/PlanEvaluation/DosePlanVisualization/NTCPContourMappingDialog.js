import React from 'react';
import { Modal, Button } from 'antd';
import PropTypes from 'prop-types';
import { ContourMappingTable } from '../../../AiModelIntegration/ContourMappingDialog';

export default class NTCPContourMappingDialog extends React.Component {
  constructor(props) {
    super(props);
    NTCPContourMappingDialog.propTypes = {
      NTCPContourMappingDialogFunctions: PropTypes.object,
      ntcpModelName: PropTypes.string,
      ntcpContourMapping: PropTypes.object,
      onMappingChanged: PropTypes.func,
    };

    props.NTCPContourMappingDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
      errorMessage: '',
      ntcpContourMapping: { ...this.props.ntcpContourMapping },
    };
  }

  componentDidUpdate(prevProps) {
    this.props.NTCPContourMappingDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    if (prevProps.ntcpContourMapping !== this.props.ntcpContourMapping) {
      this.setState({
        ntcpContourMapping: { ...this.props.ntcpContourMapping },
      });
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
      errorMessage: '',
    });
  }

  handleOk() {
    this.props.onMappingChanged(this.state.ntcpContourMapping);
    this.setVisible(false);
  }

  handleCancel() {
    this.setState({ ntcpContourMapping: { ...this.props.ntcpContourMapping } });
    this.setVisible(false);
  }

  render() {
    const lipp22Channels = [
      'Parotid Left',
      'Parotid Right',
      'Submandibular Gland Right',
      'Submandibular Gland Left',
      'Oral Cavity',
      'Pharconstructor Muscle Sup',
      'Pharconstructor Muscle Middle',
      'Pharconstructor Muscle Inferior',
    ];

    const esoScriChannels = ['Inferior pharyngeal constrictor muscle', 'Supraglottic larynx'];

    return (
      <Modal
        className="ntcpContourMappingModal"
        open={this.state.visible}
        closable={false}
        destroyOnClose={true}
        onOk={() => this.handleOk()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="ok" onClick={() => this.handleOk()}>
            Ok
          </Button>,
        ]}
        style={{ zIndex: '1001' }}
        cancelButtonProps={{ style: { display: 'none' } }}
      >
        <div className="nctpContourMappingTableDiv" style={{ overflow: 'auto', height: 'fit-content', maxHeight: '350px' }}>
          <ContourMappingTable
            channels={this.props.ntcpModelName === 'ntcpLIPPv22' ? lipp22Channels : esoScriChannels}
            mapping={this.state.ntcpContourMapping}
            onMappingChanged={(newMapping) => {
              this.setState({ ntcpContourMapping: newMapping });
            }}
          />
        </div>
        <h3 style={{ color: 'red', fontSize: '17px', marginTop: '5px' }}>
          Warning: Make sure that all the contours are matched correctly for the calculation!
        </h3>
      </Modal>
    );
  }
}
