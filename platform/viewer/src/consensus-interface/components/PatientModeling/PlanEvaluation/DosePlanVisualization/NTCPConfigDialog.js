import React from 'react';
import { Button, Modal, Menu, Space, Dropdown } from 'antd';
import ParameterSetting from '../../../PatientModeling/PlanEvaluation/DosePlanVisualization/ParameterSetting';
import PropTypes from 'prop-types';
import '../../../PatientModeling/PlanEvaluation/style/parameterSetting_styles.css';

const ntcpModelNames = ['ntcpLIPPv22', 'ntcpEsophagusStricture'];

export default class NTCPConfigDialog extends React.Component {
  constructor(props) {
    super(props);

    NTCPConfigDialog.propTypes = {
      segDataSet: PropTypes.object,
      doseDataSet: PropTypes.object,
      ntcpSettingsFunctions: PropTypes.func,
      selectedNtcpModelName: PropTypes.string,
      ntcpSettings: PropTypes.array,
      onNtcpSettingsChanged: PropTypes.func,
    };

    props.ntcpSettingsFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    this.onNtcpModelChange = this.onNtcpModelChange.bind(this);

    this.dialogkey = 0;

    this.state = {
      visible: false,
      selectedNtcpModelName: props.selectedNtcpModelName,
      ntcpSettings: structuredClone(props.ntcpSettings), // make copy
    };
  }

  componentDidUpdate(prevProps) {
    this.props.ntcpSettingsFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    if (prevProps.ntcpSettings !== this.props.ntcpSettings) {
      this.setState({
        ntcpSettings: structuredClone(ntcpSettings),
      });
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  handleOk() {
    this.props.onNtcpSettingsChanged(this.state.selectedNtcpModelName, this.state.ntcpSettings);
    this.setVisible(false);
  }

  handleCancel() {
    this.setState({
      selectedNtcpModelName: this.props.selectedNtcpModelName,
      ntcpSettings: structuredClone(this.props.ntcpSettings),
    });
    this.setVisible(false);
  }

  onNtcpModelChange(newModelName) {
    this.setState({
      selectedNtcpModelName: newModelName,
    });
  }

  renderNtcpSelectModelValueDropdown() {
    const menu = (
      <div className="ntcpSelectModelValueDiv">
        <Menu
          items={ntcpModelNames.map((item, index) => {
            return {
              key: index,
              label: <label style={{ color: 'black' }}>{item}</label>,
            };
          })}
          onClick={({ key }) => this.onNtcpModelChange(ntcpModelNames[key])}
        />
      </div>
    );
    return (
      <Space direction="vertical" style={{ marginLeft: '10px' }}>
        <Space wrap>
          <Dropdown overlay={menu} placement="bottom">
            <Button
              style={{
                backgroundColor: 'white',
                color: 'black',
                border: '1px solid grey',
                borderRadius: '2px',
                width: '250px',
              }}
            >
              {this.state.selectedNtcpModelName == null ? '---- no model selected ----' : this.state.selectedNtcpModelName}
            </Button>
          </Dropdown>
        </Space>
      </Space>
    );
  }

  renderNTCPSettingContent() {
    return (
      <div>
        <h2 className="ntcpConfigDialogTitle">NTCP configuration</h2>
        <table className="ntcpSelectModelTable">
          <tbody className="ntcpSelectModelTB">
            <tr className="ntcpSelectModelTR">
              <td className="ntcpSelectModelLabelTD">Select NTCP Model: </td>
              <td className="ntcpSelectModelValueTD">{this.renderNtcpSelectModelValueDropdown()}</td>
            </tr>
          </tbody>
        </table>
        <div className="ntcpSelectedModelParameterDiv">
          <ParameterSetting
            className="ntcpParameterSetting"
            key={this.dialogkey}
            segDataSet={this.props.segDataSet}
            ntcpModelName={this.state.selectedNtcpModelName}
            ntcpSettings={this.state.ntcpSettings}
          />
        </div>
      </div>
    );
  }

  render() {
    this.dialogkey++;
    return (
      <Modal
        className="ntcpConfigModal"
        destroyOnClose={true}
        open={this.state.visible}
        onOk={() => this.handleOk()}
        onCancel={() => this.handleCancel()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="ok" type="primary" onClick={() => this.handleOk()}>
            OK
          </Button>,
        ]}
      >
        {this.renderNTCPSettingContent()}
      </Modal>
    );
  }
}
