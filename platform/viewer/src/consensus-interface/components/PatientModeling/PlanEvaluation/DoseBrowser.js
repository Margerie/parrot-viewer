import React from 'react';
import PropTypes from 'prop-types';
import '../PlanEvaluation/style/styles.css';
import { Select } from 'antd';

export default class DoseBrowser extends React.Component {
  static propTypes = {
    doseDataSet: PropTypes.object,
    onDoseModeChange: PropTypes.func.isRequired,
    doseMode: PropTypes.number,
    onSelectedDoseChange: PropTypes.func.isRequired,
    doseIndex1: PropTypes.number,
    doseIndex2: PropTypes.number,
  };

  render() {
    if (!this.props.doseDataSet) return <div>No dose available</div>;

    const doses = this.props.doseDataSet.getDoses();
    const doseOptions = doses.map((dose, i) => ({
      label: dose.getLabel(),
      value: i,
    }));

    const doseModeOptions = [
      { label: 'Show only dose 1', value: 0 },
      { label: 'Show only dose 2', value: 1 },
      { label: 'Show dose difference', value: 2 },
    ];

    return (
      <div className="doseTableDiv">
        <div className="doseSelectionDiv">
          <Select
            className="dosefirstSelect"
            placeholder="Select dose 1"
            value={this.props.doseIndex1}
            onChange={(v) => this.props.onSelectedDoseChange(v, 1)}
            options={doseOptions}
            dropdownStyle={{ backgroundColor: 'white' }}
            style={{
              width: '210px',
            }}
          />
          <Select
            className="doseSecondSelect"
            placeholder="Select dose 2"
            value={this.props.doseIndex2}
            onChange={(v) => this.props.onSelectedDoseChange(v, 2)}
            options={doseOptions}
            dropdownStyle={{ backgroundColor: 'white' }}
            style={{
              width: '210px',
            }}
          />
          <Select
            className="doseModeSelect"
            value={this.props.doseMode}
            onChange={(v) => this.props.onDoseModeChange(v)}
            options={doseModeOptions}
            dropdownStyle={{ backgroundColor: 'white' }}
            style={{
              width: '210px',
            }}
          />
        </div>
      </div>
    );
  }
}
