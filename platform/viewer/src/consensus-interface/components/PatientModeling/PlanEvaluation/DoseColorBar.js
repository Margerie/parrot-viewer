import React from 'react';
import { render } from 'react-dom';

import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import { Modal } from 'antd';
import { useTable } from 'react-table';
import '../PlanEvaluation/style/colorbarStyles.css';

function ColorTable({ columns, data, onSelectColor }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
  });

  return (
    <table className="colorBarTable" {...getTableProps()}>
      <thead>
        {headerGroups.map(headerGroup => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map(column => (
              <th {...column.getHeaderProps()} className={column.className}>
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()} className="colorbarTbody">
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map(cell => {
                if (cell.column.id === 'color') {
                  return (
                    <td>
                      <input
                        className="cellColor"
                        type="color"
                        value={cell.value}
                        onChange={event => {
                          onSelectColor(cell.row.index, event.target.value);
                        }}
                      ></input>
                    </td>
                  );
                } else if (cell.column.id == 'levelgy') {
                  return (
                    <td {...cell.getCellProps()}>{cell.value.toFixed(2)}</td>
                  );
                } else {
                  return (
                    <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                  );
                }
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

const doseDiffColumns = [
  { Header: 'Level[%]', accessor: 'levelpercent', className: 'percentHeader' },
  { Header: 'Color', accessor: 'color', className: 'colorHeader' },
  { Header: 'Opacity', accessor: 'opacity', className: 'opcityHeader' },
];

const doseColumns = [
  { Header: 'Level[Gy]', accessor: 'levelgy', className: 'levelgyHeader' },
  { Header: 'Level[%]', accessor: 'levelpercent', className: 'percentHeader' },
  { Header: 'Color', accessor: 'color', className: 'colorHeader' },
  { Header: 'Opacity', accessor: 'opacity', className: 'opacityHeader' },
];

const percentDoseValues = [0, 25, 50, 74, 83, 90, 95, 107];
const percentDoseColors = [
  '#000000',
  '#037DFF',
  '#00007F',
  '#FF7E3D',
  '#8080C0',
  '#FFFF00',
  '#00FF00',
  '#FF0000',
];
const percentDoseOpacities = [0, 255, 255, 255, 255, 255, 255, 255];
const percentDoseDiffValues = [-25, -10, -5, 0, 5, 10, 25];
const percentDoseDiffColors = [
  '#07FBC1',
  '#0000C0',
  '#C1C0FC',
  '#000000',
  '#FCC004',
  '#FD0001',
  '#FF00FD',
];
const percentDoseDiffOpacities = [255, 255, 255, 0, 255, 255, 255];

// Calculate the color distribution
export function calculateColorColumnData(
  value,
  tabKey,
  oldDoseDiffData,
  oldDoseData
) {
  let columnData = [];
  if (tabKey == 'Dose') {
    for (let i = percentDoseValues.length - 1; i >= 0; i--) {
      let temp = value * (percentDoseValues[i] / 100);
      let row = {
        levelgy: temp,
        levelpercent: percentDoseValues[i],
        color: oldDoseData
          ? oldDoseData[oldDoseData.length - i - 1].color
          : percentDoseColors[i],
        opacity: percentDoseOpacities[i],
      };
      columnData.push(row);
    }
    return columnData;
  }

  if (tabKey == 'DoseDiff') {
    for (let i = percentDoseDiffValues.length - 1; i >= 0; i--) {
      let row = {
        levelpercent: percentDoseDiffValues[i],
        color: oldDoseDiffData
          ? oldDoseDiffData[oldDoseDiffData.length - i - 1].color
          : percentDoseDiffColors[i],
        opacity: percentDoseDiffOpacities[i],
      };
      columnData.push(row);
    }
    return columnData;
  }
}

export default class DoseColorBar extends React.Component {
  constructor(props) {
    super(props);
    props.colorbarFunctions.setVisible = isVisible => {
      this.setVisible(isVisible);
    };
    this.onValueChange = this.onValueChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      visible: false,
      selectedOption: 'Absolute',
      configSelectedOption: 'referenceValue',
      activeTab: 0,
      referenceValue: props.doseColorSettings.referenceValue,
      enteredReferenceValue: props.doseColorSettings.referenceValue,
      maxDoseValue: props.doseColorSettings.maxDoseValue,
      enteredMaxDoseValue: props.doseColorSettings.maxDoseValue,
      doseDiffData: props.doseColorSettings.doseDiffData,
      doseData: props.doseColorSettings.doseData,
    };
  }

  componentDidUpdate(prevProps) {
    this.props.colorbarFunctions.setVisible = isVisible => {
      this.setVisible(isVisible);
    };
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  handleOk() {
    this.props.onColorsChanged({
      referenceValue: this.state.referenceValue,
      maxDoseValue: this.state.maxDoseValue,
      doseData: this.state.doseData,
      doseDiffData: this.state.doseDiffData,
    });
    this.setVisible(false);
  }

  handleCancel() {
    this.setVisible(false);
  }

  onValueChange(event) {
    this.setState({
      selectedOption: event.target.value,
    });
  }

  onValueChangeConfig(event) {
    this.setState({
      configSelectedOption: event.target.value,
    });
  }

  handleSelect(index) {
    this.setState({
      activeTab: index,
    });
  }

  onReferenceValueChange(event) {
    const newValue = parseFloat(event.target.value);
    if (isNaN(event.target.value) || isNaN(newValue) || newValue < 0) {
      // not a number
      event.target.style.backgroundColor = 'red';
    } else {
      // is number
      event.target.style.backgroundColor = 'black';
      this.setState({
        referenceValue: newValue,
        doseData: calculateColorColumnData(
          newValue,
          'Dose',
          this.state.doseDiffData,
          this.state.doseData
        ),
      });
    }
    this.setState({
      enteredReferenceValue: event.target.value,
    });
  }

  onDoseMaxValueChange(event) {
    const newValue = parseFloat(event.target.value);
    if (isNaN(event.target.value) || isNaN(newValue) || newValue < 0) {
      // not a number
      event.target.style.backgroundColor = 'red';
    } else {
      // is number
      event.target.style.backgroundColor = 'black';
      this.setState({
        maxDoseValue: newValue,
        doseDiffData: calculateColorColumnData(
          newValue,
          'DoseDiff',
          this.state.doseDiffData,
          this.state.doseData
        ),
      });
    }
    this.setState({
      enteredMaxDoseValue: event.target.value,
    });
  }

  renderRadioButtonsConfig() {
    if (this.state.activeTab == 1) {
      return (
        <table className="valueConfigTable">
          <tbody className="valueConfigTb">
            <tr>
              <td>
                <label className="radiolabelRefValue">
                  <input
                    className="radioCheckItem"
                    value="referenceValue"
                    name="valueMode"
                    type="radio"
                    checked={this.state.configSelectedOption === 'referenceValue'}
                    onChange={event => this.onValueChangeConfig(event)}
                  />
                  Reference value [Gy]:
                </label>
              </td>
              <td>
                <input
                  className="referenceValueInput"
                  type="text"
                  id="inputReferenceValueId"
                  size="5"
                  onChange={event => this.onReferenceValueChange(event)}
                  value={this.state.enteredReferenceValue}
                />
              </td>
            </tr>
          </tbody>
        </table>
      );
    } else {
      return (
        <table className="valueConfigTable">
          <tbody className="valueConfigTb">
            <tr>
              <td>
                <label className="radiolabelCurrentDoseMax">
                  <input
                    className="radioCheckItem"
                    value="currentDoseMax"
                    name="valueMode"
                    type="radio"
                    checked={
                      this.state.configSelectedOption === 'currentDoseMax'
                    }
                    onChange={event => this.onValueChangeConfig(event)}
                  />
                  Maximum dose:
                </label>
              </td>
              <td>
                <input
                  className="curDoseMaxInput"
                  type="text"
                  id="curDoseMaxInputId"
                  size="5"
                  onChange={event => this.onDoseMaxValueChange(event)}
                  value={this.state.enteredMaxDoseValue}
                />
              </td>
            </tr>
          </tbody>
        </table>
      );
    }
  }

  renderRadioButtons() {
    return (
      <div className="radiobuttons">
        <div className="radio">
          <label className="radiolabelAbsolute">
            <input
              className="radioCheckItem"
              value="Absolute values"
              name="valuesMode"
              type="radio"
              checked={this.state.selectedOption === 'Absolute'}
              onChange={this.onValueChange}
            />
            Absolute values
          </label>
        </div>
        <div className="radio">
          <label className="radiolabelRelative">
            <input
              disabled={true}
              className="radioCheckItem"
              value="Relative values"
              name="valuesMode"
              type="radio"
              checked={this.state.selectedOption === 'Relative'}
              onChange={this.onValueChange}
            />
            Relative values
          </label>
        </div>
      </div>
    );
  }

  renderDoseDiffColorTable() {
    // console.log(this.state.doseDiffData[0])
    if (this.state.selectedOption == 'Absolute') {
      return (
        <div className="doseColorTableDiv">
          {' '}
          <ColorTable
            className="colorTable"
            columns={doseDiffColumns}
            data={this.state.doseDiffData}
            onSelectColor={(rowIndex, newColor) => {
              this.setState(oldState => {
                var newData = oldState.doseDiffData.map((item, i) => {
                  if (i === rowIndex) {
                    return {
                      levelpercent: item.levelpercent,
                      color: newColor,
                      opacity: item.opacity,
                    };
                  } else {
                    return item;
                  }
                });
                return {
                  doseDiffData: newData,
                };
              });
            }}
          ></ColorTable>
        </div>
      );
    }
    return {};
  }

  renderDoseColorTable() {
    if (this.state.selectedOption == 'Absolute') {
      return (
        <div className="doseColorTableDiv">
          {' '}
          <ColorTable
            className="colorTable"
            columns={doseColumns}
            data={this.state.doseData}
            onSelectColor={(rowIndex, newColor) => {
              this.setState(oldState => {
                var newData = oldState.doseData.map((item, i) => {
                  if (i === rowIndex) {
                    return {
                      levelgy: item.levelgy,
                      levelpercent: item.levelpercent,
                      color: newColor,
                      opacity: item.opacity,
                    };
                  } else {
                    return item;
                  }
                });
                return {
                  doseData: newData,
                };
              });
            }}
          ></ColorTable>
        </div>
      );
    }
    return {};
  }

  renderColorStick(tabKey) {
    if (tabKey == 'Dose') {
      return (
        <table className="colorStickDoseTableDose">
          <tbody className="colorStickDoseTbDose">
            {this.state.doseData.map((item, i) => (
              <tr className="colorBarStickDoseTrDose">
                <td
                  className="colorBarStickTd"
                  style={{
                    height:
                      i == 0
                        ? '2% '
                        : 0.98 *
                        (this.state.doseData[i - 1].levelpercent -
                          item.levelpercent) +
                        '%',
                    backgroundColor: item.color,
                  }}
                ></td>
              </tr>
            ))}
          </tbody>
        </table>
      );
    }
    if (tabKey == 'DoseDiff') {
      return (
        <table className="colorStickDoseTableDoseDiff">
          <tbody className="colorStickDoseTbDoseDiff">
            {this.state.doseDiffData.map((item, i) => (
              <tr className="doseDiffColorTR">
                <td
                  className="diffColorStickTd"
                  style={{
                    height: item.levelpercent + '%',
                    backgroundColor: item.color,
                  }}
                ></td>
              </tr>
            ))}
          </tbody>
        </table>
      );
    }
  }

  renderDoseColorContent() {
    return (
      <div className="dose-colorbar-tabs">
        <Tabs
          onSelect={(index, lastIndex, event) => {
            this.handleSelect(index);
            return true;
          }}
        >
          <TabList className="tablist">
            <Tab className="doseDiffTab" label="Dose Difference">
              Dose Difference
            </Tab>
            <Tab className="doseTab" label="Dose">
              Dose
            </Tab>
          </TabList>
          <TabPanel className="doseDiffPanel">
            <div className="doseDiffDiv">
              <div className="colorBarleft">
                {this.renderColorStick('DoseDiff')}
              </div>
              <div className="colorbarMiddle">
                {this.renderRadioButtons()}
                {this.state.activeTab == 0
                  ? this.renderDoseDiffColorTable()
                  : null}
              </div>
              <div className="colorBarRight">
                <label className="deflabel">
                  Definition of 100% __________________________________
                </label>
                {this.renderRadioButtonsConfig()}
              </div>
            </div>
          </TabPanel>
          <TabPanel className="doseColorPanel">
            <div className="doseDiv">
              <div className="colorBarleft">
                {this.renderColorStick('Dose')}
              </div>
              <div className="colorbarMiddle">
                {this.renderRadioButtons()}
                {this.state.activeTab == 1 ? this.renderDoseColorTable() : null}
              </div>
              <div className="colorBarRight">
                <label className="deflabel">
                  Definition of 100% __________________________________
                </label>
                {this.renderRadioButtonsConfig()}
              </div>
            </div>
          </TabPanel>
        </Tabs>
      </div>
    );
  }

  render() {
    // console.log("********** render");
    return (
      <Modal
        className="doseColorModal"
        visible={this.state.visible}
        onOk={() => this.handleOk()}
        onCancel={() => this.handleCancel()}
      >
        {this.renderDoseColorContent()}
      </Modal>
    );
  }
}
