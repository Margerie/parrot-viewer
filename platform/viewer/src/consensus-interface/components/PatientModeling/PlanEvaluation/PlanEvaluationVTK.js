import React from 'react';
import uniqBy from 'lodash/uniqBy';
import PropTypes from 'prop-types';
import VTKDoseViews from './VTKDoseViews';
import ImagePreviews from '../ImagePreviews.js';
import DoseBrowser from './DoseBrowser.js';
import DoseToolBar from './DoseToolBar.js';
import ContourBrowser from '../ContourBrowser.js';
import DataSet from '../../../data/DataSet.js';
import ViewsApi from '../Services/ViewsApi.js';
import SegSelection from '../SegSelection.js';
import PlanVisualization from './DosePlanVisualization/PlanVisualization';

import '../PlanEvaluation/style/styles.css';

export default class PlanEvaluation extends React.Component {
  constructor(props) {
    super(props);

    PlanEvaluation.propTypes = {
      StudyInstanceUID: PropTypes.string.isRequired,
    };

    this.dataSet = DataSet.getInstance();

    this.dataListener = (event) => this.dataSetListener(event);
    this.dataSet.addListener(this.dataListener);

    this.imageApi = null;

    this.promiseSeriesInstanceUID = null;
    this.promiseSecondarySeriesInstanceUID = null;
    this.promiseSEGSeriesUID = null;
    this.promiseSEGSOPUID = null;

    const doses = this.dataSet.getDoseDataSet()?.getDoses();
    const doseIndex1 = doses?.length > 0 ? 0 : null;
    const doseIndex2 = doses?.length > 1 ? 1 : null;

    this.state = {
      activeSeriesInstanceUID: null,
      activeSecondarySeriesInstanceUID: null,
      imageDataSet: this.dataSet.getImageDataSet(),
      doseDataSet: this.dataSet.getDoseDataSet(),
      activeSEGSeriesUID: null,
      activeSEGSOPUID: null,
      segs: null,
      StudyInstanceUID: this.props.StudyInstanceUID,
      study: this.dataSet.getStudy(),
      previewsOn: true,
      layout: 3,
      doseMode: 2,
      doseIndex1: doseIndex1,
      doseIndex2: doseIndex2,
      doseScaleFactor: Array(doses?.length).fill(1.0),
    };
  }

  dataSetListener(event) {
    if (event.type == 'created') {
      // In case not already loaded when calling constructor
      switch (event.target) {
        case this.props.StudyInstanceUID:
          this.setState({ study: this.dataSet.getStudy() });
          break;
        case this.promiseSeriesInstanceUID:
          break;
        case this.promiseSecondarySeriesInstanceUID:
          break;
        case this.promiseSEGSOPUID:
          this.setState({
            segs: this.dataSet.getSegDataSet().getSegs(this.promiseSEGSOPUID),
            activeSEGSeriesUID: this.promiseSEGSeriesUID,
            activeSEGSOPUID: this.promiseSEGSOPUID,
          });
          break;
        default:
      }
    }
  }

  // modalities: 'CT', 'MR', 'PT'
  getSeriesInstanceUID(st) {
    if (!st) return null;

    const allSeries = uniqBy(st, 'SeriesInstanceUID');
    if (!allSeries.length) return null;

    const CTUIds = allSeries.filter((study) => study.modalities == 'CT').map((study) => study.SeriesInstanceUID);

    let CTUId;
    if (CTUIds.length) CTUId = CTUIds[0];
    else CTUId = allSeries.filter((study) => study.modalities == 'MR' || study.modalities == 'PT').map((study) => study.SeriesInstanceUID)[0];

    return CTUId;
  }

  getSeriesInstanceUIDs() {
    if (!this.state.study) return null;

    return uniqBy(this.state.study, 'SeriesInstanceUID')
      .filter((study) => study.modalities == 'CT' || study.modalities == 'MR' || study.modalities == 'PT')
      .map((study) => study.SeriesInstanceUID);
  }

  handleActiveSeriesInstanceUIDSelected(activeSeriesInstanceUID) {
    const imageDataSet = this.dataSet.getImageDataSet();

    if (imageDataSet && imageDataSet.getImage(activeSeriesInstanceUID)) {
      this.setState({ imageDataSet, activeSeriesInstanceUID });

      if (this.imageApi) ViewsApi.getInstance().unregisterApi(this.imageApi);

      this.imageApi = imageDataSet.getImage(activeSeriesInstanceUID).getImageProperties().getApi();

      ViewsApi.getInstance().registerApi(this.imageApi);
    }
  }

  handleActiveSecondarySeriesInstanceUIDSelected(activeSecondarySeriesInstanceUID) {
    let layout = this.state.layout;

    if (!activeSecondarySeriesInstanceUID) layout = 3;

    this.setState({ layout, activeSecondarySeriesInstanceUID });
  }

  handlesSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID) {
    this.promiseSEGSOPUID = activeSEGSOPUID;

    const segs =
      activeSEGSOPUID == -1
        ? {
            getSegs: () =>
              this.dataSet
                .getSegDataSet()
                .getAllSegs()
                .map((s) => s.getSegs())
                .flat(),
          }
        : this.dataSet.getSegDataSet().getSegs(activeSEGSOPUID);

    if (segs) this.setState({ segs, activeSEGSeriesUID, activeSEGSOPUID });
  }

  setUserPosition(userPosition) {
    if (this.state.imageDataSet) this.state.imageDataSet.getImages().forEach((image) => image.getImageProperties().setUserPosition(userPosition));
  }

  renderPreviews() {
    if (this.state.study) {
      return (
        <ImagePreviews
          className="imagePreviews"
          clickEnabled={true}
          activeSeriesInstanceUID={this.state.activeSeriesInstanceUID}
          activeSecondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
          SeriesInstanceUIDs={this.getSeriesInstanceUIDs()}
          onPrimaryItemClick={(activeSeriesInstanceUID) => {
            this.handleActiveSeriesInstanceUIDSelected(activeSeriesInstanceUID);
          }}
          onSecondaryItemClick={(activeSeriesInstanceUID) => {
            this.handleActiveSecondarySeriesInstanceUIDSelected(activeSeriesInstanceUID);
          }}
        />
      );
    } else {
      return <h2 style={{ color: 'red', width: '400px', textAlign: 'center' }}>No study selected</h2>;
    }
  }

  renderImageView() {
    if (this.state.activeSeriesInstanceUID) {
      if (this.state.imageDataSet && this.state.imageDataSet.getImage(this.state.activeSeriesInstanceUID)) {
        return (
          <VTKDoseViews
            id="doseViewsId"
            SeriesInstanceUID={this.state.activeSeriesInstanceUID}
            secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
            doseIndex1={this.state.doseIndex1}
            doseIndex2={this.state.doseIndex2}
            doseDataSet={this.state.doseDataSet}
            segDataSet={this.state.segs}
            imageDataSet={this.state.imageDataSet}
            layout={this.state.layout}
            doseMode={this.state.doseMode}
            doseScaleFactor={this.state.doseScaleFactor}
          />
        );
      } else {
        return <div>Loading...</div>;
      }
    } else return <h2 style={{ color: 'red', width: '400px', textAlign: 'center' }}>No series selected</h2>;
  }

  storeApis(api, apis) {
    this.api = api;
    this.vtkViewsApis = apis;
  }

  componentWillUnmount() {
    this.dataSet.removeListener(this.dataListener);
    if (this.imageApi) ViewsApi.getInstance().unregisterApi(this.imageApi);
  }

  render() {
    let previews = null;

    if (this.state.previewsOn) previews = this.renderPreviews();

    const imageView = this.renderImageView();

    let dosePanel = <div className="dosePanelDiv"></div>;
    let contoursPanel = <div className="contoursPanelDiv"></div>;

    if (this.state.study) {
      // panel for RT doses
      let doseBrowser = null;
      doseBrowser = (
        <DoseBrowser
          doseDataSet={this.state.doseDataSet}
          doseMode={this.state.doseMode}
          doseIndex1={this.state.doseIndex1}
          doseIndex2={this.state.doseIndex2}
          onDoseModeChange={(newDoseMode) => {
            this.setState({ doseMode: newDoseMode });
          }}
          onSelectedDoseChange={(doseIndex, index) => {
            if (index === 1) {
              this.setState({ doseIndex1: doseIndex });
            } else {
              this.setState({ doseIndex2: doseIndex });
            }
          }}
        />
      );
      dosePanel = doseBrowser;
      // Panel for contours
      let contourBroswer = null;
      if (!this.state.segs) {
        contoursPanel = <div className="SegPanelContent">Loading...</div>;
      } else {
        contourBroswer = (
          <ContourBrowser
            className="contourBrowser"
            segDataSet={this.state.segs}
            showEdit={false}
            setUserPosition={(userPosition) => this.setUserPosition(userPosition)}
            isPlanEvaluation={true}
          />
        );
      }

      contoursPanel = (
        <div className="SegPanelContent">
          <SegSelection
            study={this.state.study}
            onSelectedSEG={(activeSEGSeriesUID, activeSEGSOPUID) => {
              this.handlesSelectedSEG(activeSEGSeriesUID, activeSEGSOPUID);
            }}
          />
          {contourBroswer}
        </div>
      );
    } else if (this.state.StudyInstanceUID) {
      dosePanel = <div>Loading...</div>;
    }

    return (
      <div className="PlanEvaluationVTK">
        <div className="PlanEvaluationVTKToolbar">
          <DoseToolBar secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID} imageDataSet={this.state.imageDataSet} />
        </div>
        <div className="PlanEvaluationVTKMain">
          <div className="dosePanel">
            {dosePanel}
            {contoursPanel}
          </div>
          <div className="ImageViewsPlanEvaluation">
            <div className={this.state.previewsOn ? 'ImageView' : 'ImageView extended'}>{imageView}</div>
            <div className={this.state.previewsOn ? 'ImagePreviewsBottom extended' : 'ImagePreviewsBottom'}>
              <div className="previewsList">{this.state.previewsOn && previews}</div>
            </div>
          </div>
          <div className="dose-plan">
            <PlanVisualization
              secondarySeriesInstanceUID={this.state.activeSecondarySeriesInstanceUID}
              SeriesInstanceUID={this.state.activeSeriesInstanceUID}
              doseDataSet={this.state.doseDataSet}
              doseIndex1={this.state.doseIndex1}
              doseIndex2={this.state.doseIndex2}
              segDataSet={this.state.segs}
              doseScaleFactor={this.state.doseScaleFactor}
              onDoseScaleFactor={(newDoseScaleFactor) => {
                console.log('*** New dose scale factor is ' + newDoseScaleFactor);
                this.setState({ doseScaleFactor: newDoseScaleFactor });
              }}
            ></PlanVisualization>
          </div>
        </div>
      </div>
    );
  }
}
