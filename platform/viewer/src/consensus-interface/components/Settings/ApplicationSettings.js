
import React from "react";
import {SETTING_NAMES, MainSettings} from '../../utils/MainSettings.js';

import './styles.css'

export default class ApplicationSettings extends React.Component {
  constructor(props) {
    super(props);
    
    this.settings = new MainSettings();
    
    this.state = {PACSURL: this.settings.get(SETTING_NAMES.PACS_URL)};
  }
  
  onSaveChanges() {
    const urlInput = document.getElementById(SETTING_NAMES.PACS_URL);
    this.settings.set(SETTING_NAMES.PACS_URL, urlInput.value);
    
    this.setState({PACSURL: this.settings.get(SETTING_NAMES.PACS_URL)});
  }
  
  render() {
    return (
      <div>
        <form>
          <label for={SETTING_NAMES.PACS_URL}>PACS URL: </label>
          <input type="text" id={SETTING_NAMES.PACS_URL} name={SETTING_NAMES.PACS_URL} defaultValue={this.settings.get(SETTING_NAMES.PACS_URL)}></input><br></br><br></br>
          <input type="submit" value="Save changes" onClick={() => this.onSaveChanges()}></input>
        </form>
      </div>
    );
  }
}

