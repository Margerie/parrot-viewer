import React, { useMemo } from 'react';
import { Select, Modal, Button, Space } from 'antd';
import { useTable } from 'react-table';
import PropTypes from 'prop-types';
import { BsArrowDownCircle } from 'react-icons/bs';
import './styles.css';
import DataSet from '../../data/DataSet';
import uniqBy from 'lodash/uniqBy';

export const ContourMappingTable = ({ channels, mapping, onMappingChanged }) => {
  const headerStyle = {
    border: 'solid 1px black',
    textAlign: 'center',
    fontSize: '16px',
    padding: '2px',
    background: 'grey',
    color: 'white',
  };
  const tdStyle = {
    padding: '1px',
    border: 'solid 1px grey',
    fontSize: '12px',
    textAlign: 'left',
    cursor: 'default',
    color: 'white',
    width: '350px',
  };

  const contours = [];
  const dataset = DataSet.getInstance();
  uniqBy(dataset.getStudy(), 'SeriesInstanceUID')
    .filter((study) => study.modalities == 'SEG' || study.modalities == 'RTSTRUCT')
    .forEach((study) => {
      const seriesDescription = study.SeriesDescription;
      const segs = dataset.getSegDataSet().getSegs(study.SOPInstanceUID).getSegs();
      segs.forEach((seg) => {
        contours.push({ series: seriesDescription, segment: seg.getLabel() });
      });
    });

  const contourOptions = contours.map((c, i) => {
    return {
      label: c.segment + ' (' + c.series + ')',
      value: i,
    };
  });

  // The Select component for contours
  const ContourSelect = ({ onContourSelected, contour }) => {
    let value = contour ? contours.findIndex((c) => c.series === contour.series && c.segment == contour.segment) : null;
    if (value == -1) value = null;
    return (
      <Select
        suffixIcon={<BsArrowDownCircle style={{ color: 'yellow' }} />}
        className="contourMappingSelect"
        showSearch
        placeholder={<div style={{ color: 'yellow' }}>---------</div>}
        optionFilterProp="children"
        dropdownStyle={{ backgroundColor: 'white' }}
        onSelect={(value) => onContourSelected(contours[value])}
        onSearch={() => {}}
        filterOption={(input, option) => (option?.label ?? '').toLowerCase().includes(input.toLowerCase())}
        options={contourOptions}
        value={value}
        style={{ width: '350px', textAlign: 'left' }}
      />
    );
  };

  const columns = useMemo(
    () => [
      {
        Header: 'Input channel',
        accessor: 'channel',
      },
      {
        Header: 'Contour',
        accessor: 'contour',
        style: { overflow: 'visible' },
        Cell: (cell) => {
          return (
            <ContourSelect
              className="contoursSelectTD"
              contour={cell.value}
              onContourSelected={(contour) => {
                const channel = cell?.row?.original.channel;
                const oldContour = mapping[channel];
                if (!oldContour || oldContour.series != contour.series || oldContour.segment != contour.segment) {
                  mapping[channel] = contour;
                  onMappingChanged(mapping);
                }
              }}
            />
          );
        },
      },
    ],
    [onMappingChanged]
  );

  mapping = mapping ?? {};
  const data = Object.getOwnPropertyNames(mapping).map((channel) => {
    return { channel: channel, contour: mapping[channel] };
  });
  channels.forEach((channel) => {
    if (!mapping.hasOwnProperty(channel)) {
      data.push({ channel: channel, contour: null });
    }
  });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({
    columns,
    data,
  });
  return (
    <table className="contourMappingTable" {...getTableProps()} style={{ width: '100%', overflow: 'auto' }}>
      <thead>
        {headerGroups.map((headerGroup) => (
          <tr className="contourMappingTabHeaderTR" {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th style={headerStyle} className="contourmappingHeaderTH" {...column.getHeaderProps()}>
                {column.render('Header')}
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody className="contourMappingTableTbody" {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()} className="contourMappingTableTR">
              {row.cells.map((cell) => {
                return (
                  <td {...cell.getCellProps()} style={tdStyle}>
                    <div>{cell.render('Cell')}</div>
                  </td>
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default class ContourMappingDialog extends React.Component {
  constructor(props) {
    super(props);
    ContourMappingDialog.propTypes = {
      model: PropTypes.object,
      onModelModified: PropTypes.func,
      contourMappingDialogFunctions: PropTypes.object,
    };

    props.contourMappingDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);

    this.state = {
      visible: false,
      errorMessage: '',
      model: { ...this.props.model },
    };
  }

  componentDidUpdate(prevProps) {
    this.props.contourMappingDialogFunctions.setVisible = (isVisible) => this.setVisible(isVisible);
    if (prevProps.model !== this.props.model) {
      this.setState({
        model: { ...this.props.model },
      });
    }
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
      errorMessage: '',
    });
  }

  handleOk() {
    this.props.onModelModified(this.state.model);
    this.setVisible(false);
  }

  handleCancel() {
    this.setState({ model: { ...this.props.model } });
    this.setVisible(false);
  }

  render() {
    return (
      <Modal
        className="contourMappingModal"
        open={this.state.visible}
        closable={false}
        destroyOnClose={true}
        onOk={() => this.handleOk()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="ok" onClick={() => this.handleOk()}>
            Ok
          </Button>,
        ]}
        style={{ zIndex: '1001' }}
        cancelButtonProps={{ style: { display: 'none' } }}
      >
        <Space direction="vertical">
          <div style={{ textAlign: 'center', backgroundColor: 'grey', fontWeight: 'bold' }}>Target volumns</div>
          <div className="tarMappingDiv" style={{ overflow: 'auto', height: 'fit-content', maxHeight: '210px' }}>
            <ContourMappingTable
              channels={this.state.model.tarChannels}
              mapping={this.state.model.tarChannelMapping}
              onMappingChanged={(mapping) => {
                this.setState({ model: { ...this.state.model, tarChannelMapping: mapping } });
              }}
            />
          </div>
          <div style={{ textAlign: 'center', backgroundColor: 'grey', fontWeight: 'bold' }}>Organs at risk</div>
          <div className="oarMappingDiv" style={{ overflow: 'auto', height: 'fit-content', maxHeight: '210px' }}>
            <ContourMappingTable
              channels={this.state.model.oarChannels}
              mapping={this.state.model.oarChannelMapping}
              onMappingChanged={(mapping) => {
                this.setState({ model: { ...this.state.model, oarChannelMapping: mapping } });
              }}
            />
          </div>
        </Space>
      </Modal>
    );
  }
}
