import React from 'react';
import { MainSettings, SETTING_NAMES } from '../../utils/MainSettings';
import ModifyModelInformationDialog from './ModifyModelInformationDialog';
import RunPredictionDialog from './RunPredictDialog';
import { AiModelsTable } from './AiModelsTable';
import DataSet from './../../data/DataSet.js';
import PropTypes from 'prop-types';

import './styles.css';

// ************ api ****************
async function saveAiModelInformation(modelInfos) {
  const response = await fetch('/aimodels/modelinfo/store', {
    method: 'POST',
    body: JSON.stringify(modelInfos),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status == 200) {
    const data = (await response.json()).message;
    return data;
  } else {
    const text = await response.text();
    var error = text;
    try {
      error = JSON.parse(text).message;
    } catch {}
    throw error;
  }
}

async function loadAiModelInformation() {
  const response = await fetch('/aimodels/modelinfo/load', {
    method: 'POST',
    body: '{}',
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status == 200) {
    const data = await response.json();
    return data;
  } else {
    const text = await response.text();
    var error = text;
    try {
      error = JSON.parse(text).message;
    } catch {}
    throw error;
  }
}
// ********************************************************************

export default class AiModelManagement extends React.Component {
  constructor(props) {
    super(props);

    AiModelManagement.propTypes = {
      activeStudyInstanceUID: PropTypes.string,
      setStudy: PropTypes.func,
      setActiveTabID: PropTypes.func,
    };

    this.dataSet = DataSet.getInstance();

    this.dataListener = (event) => this.dataSetListener(event);
    this.dataSet.addListener(this.dataListener);

    const settings = new MainSettings();
    const pacsURL = settings.get(SETTING_NAMES.PACS_URL);

    this.state = {
      aiModels: [],
      selectedModel: undefined,
      modelInfoErrorMessage: null,
      study: this.dataSet.getStudy(),
    };

    this.dicomDataLoader = DataSet.getInstance().getDataLoader();
    this.dicomDataLoader.setPACSURL(pacsURL);
    this.dicomDataLoader.enable();

    this.modifyModelDialogFunction = {};
    this.runPredictionDialogFunctions = {};

    loadAiModelInformation()
      .then((modelInfo) => this.setState({ aiModels: modelInfo }))
      .catch((error) => this.setState({ modelInfoErrorMessage: error }));
  }

  dataSetListener(event) {
    if (event.type == 'created') {
      // In case not already loaded when calling constructor
      switch (event.target) {
        case this.props.activeStudyInstanceUID:
          this.setState({
            study: this.dataSet.getStudy(),
          });
          break;
        default:
      }
    }
  }

  componentWillUnmount() {
    this.dataSet.removeListener(this.dataListener);
  }

  handleModifyModel(rowId) {
    const model = this.state.aiModels[rowId];
    this.setState({ selectedModel: model });
    this.modifyModelDialogFunction.setVisible(true);
  }

  handleRunPredictionModel(rowId) {
    const model = this.state.aiModels[rowId];
    this.setState({ selectedModel: model });
    this.runPredictionDialogFunctions.setVisible(true);
  }

  handleDeleteAiModel(rowId) {
    const copyModels = this.state.aiModels.slice();
    copyModels.splice(rowId, 1);
    this.setState({ aiModels: copyModels, modelInfoErrorMessage: null });
    saveAiModelInformation(copyModels).catch((error) => this.setState({ modelInfoErrorMessage: error }));
  }

  handleAddNewModel() {
    const newModel = {
      name: 'No',
      author: 'No',
      organisation: 'No',
      category: 'No',
      remark: 'No',
      builtin: false,
      contourMapping: {},
      modelPaths: {
        predictionPath: '',
        temporaryDirectoryPath: 'c:\\Temp',
      },
    };
    const copyModels = this.state.aiModels.slice();
    copyModels.push(newModel);
    this.setState({ aiModels: copyModels, modelInfoErrorMessage: null });
    saveAiModelInformation(copyModels).catch((error) => this.setState({ modelInfoErrorMessage: error }));
  }

  handleSelectedModelModified(modifiedModel) {
    const copyModels = this.state.aiModels.slice();
    const id = copyModels.findIndex((m) => m == this.state.selectedModel);
    if (id !== -1) {
      copyModels[id] = modifiedModel;
      this.setState({ aiModels: copyModels, modelInfoErrorMessage: null });
      saveAiModelInformation(copyModels).catch((error) => this.setState({ modelInfoErrorMessage: error }));
    }
  }

  renderAimodelTable() {
    return (
      <AiModelsTable
        {...this.props}
        aiModels={this.state.aiModels}
        onClickModifyModel={(mr) => this.handleModifyModel(mr)}
        onClickRunPrediction={(mr) => this.handleRunPredictionModel(mr)}
        onClickDeleteAiModel={(mr) => this.handleDeleteAiModel(mr)}
        onClickAddNewModel={() => this.handleAddNewModel()}
        canPredict={this.dataSet.getStudy() ? true : false}
      />
    );
  }

  render() {
    return (
      <div className="AiModelManagementDiv">
        {this.state.study ? (
          <h3 style={{ color: '#F28227' }}>Seleted Patient Study: {this.state.study[0].PatientName}</h3>
        ) : (
          <h3 className="aimodelErrorMessage">No study loaded. You must first load a study in Study Management before you can run prediction models.</h3>
        )}
        {this.state.modelInfoErrorMessage ? <h3 className="aimodelErrorMessage">{this.state.modelInfoErrorMessage}</h3> : <div />}
        <div className="aimodelsPageDiv">
          <div className="aimodelTableDiv">{this.renderAimodelTable()}</div>
        </div>
        <ModifyModelInformationDialog
          className="addNewModelDialog"
          modifyModelDialogFunction={this.modifyModelDialogFunction}
          onModelModified={(modifiedModel) => {
            this.handleSelectedModelModified(modifiedModel);
          }}
          model={this.state.selectedModel}
        />
        <RunPredictionDialog
          className="runPredictionDialog"
          runPredictionDialogFunctions={this.runPredictionDialogFunctions}
          activeStudyInstanceUID={this.props.activeStudyInstanceUID}
          onModelModified={(modifiedModel) => {
            this.handleSelectedModelModified(modifiedModel);
          }}
          model={this.state.selectedModel}
          study={this.state.study}
        />
      </div>
    );
  }
}
