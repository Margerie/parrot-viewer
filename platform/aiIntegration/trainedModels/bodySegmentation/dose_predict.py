import os
import os.path
import json
import sys
import torch
# Conversion imports
from dicom_to_nii import convert_ct_struct_dicom_to_nii
from nii_to_dicom import convert_dose_nii_to_dicom
# Import AI model
from modular_hdunet import modular_hdunet
from torch.nn import MaxPool3d, ReLU
# AI MONAI libraries
from monai.networks.nets import UNet
from monai.networks.layers import Norm
from monai.inferers import sliding_window_inference
from monai.data import CacheDataset, DataLoader, Dataset, decollate_batch, NibabelReader
from monai.utils import first
from monai.transforms import (
    EnsureChannelFirstd,
    Compose,
    CropForegroundd,
    ScaleIntensityRanged,
    Invertd,
    AsDiscreted,
    ToTensord,
    ConcatItemsd,
    ThresholdIntensityd,
    RemoveSmallObjectsd,
    KeepLargestConnectedComponentd,
    Activationsd
)
# Preprocessing
from preprocessing import LoadImaged, Separate_structsd
# Postprocessing
from postprocessing import SaveImaged, add_contours_exist
import matplotlib.pyplot as plt
import numpy as np

# +++++++++++++++++ Conversion imports ++++++++++++++++++++
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.abspath(".."))
# +++++++++++++++++ Conversion imports ++++++++++++++++++++

# Directories
patient_id = "ANON821"
dir_ct_dicom = r'../../../../NAS_database/clean_databases/Head_and_neck/HAN_PBS/HAN_PT_db/'
dir_ct_dicom_nii = r'../../../../NAS_database/clean_databases/Head_and_neck/HAN_PBS/HAN_PT_nii/'
dir_ct_nii = r'../attempt_dose/'
dir_prediction_nii = r'../attempt_dose/'
dir_prediction_dicom = r'../attempt_dose/'

model_path = os.path.join('/test','monai_attempt','Code','Dose_prediction_pt','log_no_pk',"best_metric_model_0.pth")

# ROI list
oar_list = ['BODY','Brainstem', 'Esophagus_upper', 'GlotticArea', 'OralCavity', 'Parotid_L',
 'Parotid_R', 'PharConsInf', 'PharConsMid', 'PharConsSup', 'Submandibular_L', 'Submandibular_R',
 'SpinalCord', 'SupraglotLarynx']
tv_list = ['CTV_7000', 'CTV_5425']

# Conversion DICOM to nii
if not os.path.exists(os.path.join(dir_ct_nii, patient_id)):
    os.makedirs(os.path.join(dir_ct_nii, patient_id))
refCT = convert_ct_struct_dicom_to_nii(dir_dicom = os.path.join(dir_ct_dicom, patient_id), dir_nii = os.path.join(dir_ct_nii, patient_id), oar_list = oar_list, tv_list = tv_list, newvoxelsize = None)#[3,3,3])

# Dictionary with patient to predict
#test_Data = [{'image':os.path.join(dir_ct_dicom_nii,patient_id,'nii_files_3.0_3.0_3.0','ct.nii.gz'),
#              'tv_together':os.path.join(dir_ct_dicom_nii,patient_id,'nii_files_3.0_3.0_3.0','struct_tv.nii.gz'),
#              'oar_together':os.path.join(dir_ct_dicom_nii,patient_id,'nii_files_3.0_3.0_3.0','struct_oar.nii.gz'),
#              'patient_name':patient_id
#              }]
test_Data = [{'image':os.path.join(dir_ct_nii,patient_id,'ct.nii.gz'),
               'tv_together':os.path.join(dir_ct_nii,patient_id,'struct_tv.nii.gz'),
               'oar_together':os.path.join(dir_ct_nii,patient_id,'struct_oar.nii.gz'),
               'patient_name':patient_id
            }]

# Transformations
test_pretransforms = Compose(
    [
        LoadImaged(keys=["image",'tv_together','oar_together']),
        EnsureChannelFirstd(keys=["image",'tv_together','oar_together']),
        # ThresholdIntensityd(keys=["image"], threshold=1560, above=False, cval=1560),
        ScaleIntensityRanged(
            keys=["image"], a_min=-1000, a_max=1560,
            b_min=0.0, b_max=1.0, clip=True,
        ),
        Separate_structsd(keys=["oar_together"], numstruct = 13),
        ToTensord(keys=["oar_split"]),
        ConcatItemsd(keys=['image',"tv_together","oar_split"],name="input",dim=0),
        ToTensord(keys=["input","image","tv_together"])
    ]
)
test_posttransforms = Compose(
    [
        # Activationsd(keys="pred", softmax=True),
        # Invertd(
        #     keys="pred",  # invert the `pred` data field, also support multiple fields
        #     transform=test_pretransforms,
        #     orig_keys="image",  # get the previously applied pre_transforms information on the `img` data field,
        #                       # then invert `pred` based on this information. we can use same info
        #                       # for multiple fields, also support different orig_keys for different fields
        #     nearest_interp=False,  # don't change the interpolation mode to "nearest" when inverting transforms
        #                            # to ensure a smooth output, then execute `AsDiscreted` transform
        #     to_tensor=True,  # convert to PyTorch Tensor after inverting
        # ),
        # AsDiscreted(keys="pred",  argmax=True, to_onehot=2, threshold=0.5),
        # KeepLargestConnectedComponentd(keys="pred",is_onehot=True),
        SaveImaged(keys="pred_dose", meta_keys="image_meta_dict",  output_postfix="RTDose",separate_folder=False, output_dir=os.path.join(dir_prediction_nii,patient_id), resample=False)
    ]
)

# Define DataLoader using MONAI, CacheDataset needs to be used
test_ds = CacheDataset(data=test_Data, transform=test_pretransforms, num_workers=0)
test_loader = DataLoader(test_ds, batch_size=1, shuffle=True, num_workers=0)

# check_ds = Dataset(data=test_Data, transform=test_pretransforms)
# check_loader = DataLoader(check_ds, batch_size=1)

device = torch.device("cuda:0")
model_param = dict(
            base_num_filter=15,
            num_blocks_per_stage_encoder=2,
            num_stages=4,
            pool_kernel_sizes=((2, 2, 2), (2, 2, 2), (2, 2, 2), (2, 2, 2)),
            conv_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
            conv_bottleneck_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
            num_blocks_per_stage_decoder=None,
            padding='same',
            num_steps_bottleneck=4,
            expansion_rate=1,
            pooling_type=MaxPool3d,
            pooling_kernel_size=(2, 2, 2),
            nonlin=ReLU
        )

model = modular_hdunet(**model_param)
trained_model_dict = torch.load(model_path)
model.load_state_dict(trained_model_dict['state_dict'])
del trained_model_dict
# model.load_state_dict(torch.load(model_path))
model = model.to(device)
# print("MODEL",model)

model.eval()
with torch.no_grad():
    for d in test_loader:
        images = d["input"].to(device)
        print("d['image'].shape",d["image"].shape,"d['tv_together'].shape",d["tv_together"].shape,"d['oar_split'].shape",d["oar_split"].shape)
        d['pred_dose'] = sliding_window_inference(inputs=images, roi_size=(96,96,64),sw_batch_size=1,  predictor = model)
        print("post dose prediction")
        d['pred_dose'] = [test_posttransforms(i) for i in decollate_batch(d)]        
        print("post save")
        model.cpu()

# add_contours_exist(patient_id=patient_id, preddir = dir_prediction_nii, refCT = refCT)
# Conversion DICOM to nii
convert_dose_nii_to_dicom(patient_id=patient_id, dicomctdir = os.path.join(dir_ct_dicom,patient_id), preddir = dir_prediction_nii, dicomdir = os.path.join(dir_prediction_dicom,patient_id), predicted_structures=['BODY'], refCT = refCT)
