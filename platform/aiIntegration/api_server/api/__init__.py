from flask import Blueprint

bp = Blueprint('api', __name__)

from api_server.api import prediction  # noqa
from api_server.api import management  # noqa
from api_server.api import modelinfo  # noqa
from api_server.api import ntcpsettings  # noqa